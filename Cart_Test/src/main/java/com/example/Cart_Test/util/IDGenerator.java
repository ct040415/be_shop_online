package com.example.Cart_Test.util;

public class IDGenerator {
    private String prefix;
    private int counter;

    public IDGenerator(String prefix, int counter) {
        this.prefix = prefix;
        this.counter = counter; // Bắt đầu từ 1
    }

    public String generateNextID() {
        String formattedCounter = String.format("%06d", counter);
        String nextID = prefix + formattedCounter;
        counter++;
        return nextID;
    }

    public void resetCounter() {
        counter = 1;
    }
}
