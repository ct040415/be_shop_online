package com.example.Cart_Test.dto.request;

import com.example.Cart_Test.util.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTOChangePassRequest {

    @NotBlank(message = "Email cannot be empty")
    @Email(message = "Invalid email format")
    @Size(max = Constants.EMAIL_MAX_LENGTH, message = "Email must not exceed {max} characters")
    private String email;

    @NotBlank(message = "Current password cannot be empty")
    @Size(max = Constants.PASSWORD_MAX_LENGTH, min = Constants.PASSWORD_MIN_LENGTH, message = "Current password must be between {min} and {max} characters")
    private String currentPassword;

    @NotBlank(message = "New password cannot be empty")
    @Size(max = Constants.PASSWORD_MAX_LENGTH, min = Constants.PASSWORD_MIN_LENGTH, message = "New password must be between {min} and {max} characters")
    private String newPassword;
}
