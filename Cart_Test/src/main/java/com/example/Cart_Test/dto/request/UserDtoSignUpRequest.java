package com.example.Cart_Test.dto.request;

import com.example.Cart_Test.util.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserDtoSignUpRequest {
    @NotBlank(message = "Username cannot be empty")
    @Size(max = Constants.NAME_MAX_LENGTH, min = Constants.NAME_MIN_LENGTH, message = "Username must be between {min} and {max} characters")
    private String userName;

    @NotBlank(message = "Password cannot be empty")
    @Size(max = Constants.PASSWORD_MAX_LENGTH, min = Constants.PASSWORD_MIN_LENGTH, message = "Password must be between {min} and {max} characters")
    private String password;

    @NotBlank(message = "Email cannot be empty")
    @Email(message = "Invalid email format")
    @Size(max = Constants.EMAIL_MAX_LENGTH, message = "Email must not exceed {max} characters")
    private String email;

    @Pattern(regexp = Constants.PATTERN_PHONENUMBER, message = "Invalid phone number format")
    @Size(min = Constants.PHONENUMBER_MIN_LENGTH, max = Constants.PHONENUMBER_MAX_LENGTH, message = "Phone number must be between {min} and {max} characters")
    private String phoneNumber;
    private String address;
    private String gender;
    private String avatar;
    private String background;
}
