package com.example.Cart_Test.dto.response;

import lombok.*;

import java.time.Instant;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserDTOTokenResponse {
    private String idUser;
    private List<String> roles;
    private String accessToken;
    private String token;
    private Instant expirationTime;
}
