package com.example.Cart_Test.controller;

import com.example.Cart_Test.dto.request.*;
import com.example.Cart_Test.dto.response.JwtResponse;
import com.example.Cart_Test.dto.response.ObjectResponse;
import com.example.Cart_Test.entity.postgres.RefreshToken;
import com.example.Cart_Test.entity.postgres.UserInfo;
import com.example.Cart_Test.services.postgres.UserService;
import com.example.Cart_Test.services.postgres.impl.JwtService;
import com.example.Cart_Test.services.postgres.impl.RefreshTokenService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Controller
@RestController
@RequestMapping(path = "/api/v2/users/")
@Validated
public class UserController {
    private Long loginTimeMillis;
    @Autowired
    private UserService userService;
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private RefreshTokenService refreshTokenService;

    //======================================== register ================================
    @PostMapping(path = "/signupUser")
    public ResponseEntity<?> signupUser(@Valid @RequestBody UserDtoSignUpRequest userDto, BindingResult result) {
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        return userService.signUpUser(userDto);
    }

    @PostMapping(path = "/signupAdmin")
    public ResponseEntity<?> signupAdmin(@Valid @RequestBody UserDtoSignUpRequest userDto, BindingResult result) {
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        return userService.signupByAdmin(userDto);
    }

    //======================================== login ================================
    @PostMapping(path = "user/login")
    public ResponseEntity<?> loginUser(@Valid @RequestBody UserDtoLoginRequest loginDto, BindingResult result){
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        return userService.loginUser(loginDto);
    }

    //    ==================================Update User============================================
    @PutMapping("/updateUser")
    @PreAuthorize("hasRole('1')")
    public ResponseEntity<?> updateUser(@Valid @RequestBody UserDTOInfoRequest userDTOInfoRequest, BindingResult result
    ){
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        return userService.updateUser(userDTOInfoRequest);
    }

    //    ==================================Change Pass============================================
    @PutMapping("/changePassword")
  //  @PreAuthorize("(hasRole('0') and !#userDTOChangePassRequest.isDelete())")
    public ResponseEntity<?> changePasswordByEmail(@Valid @RequestBody UserDTOChangePassRequest userDTOChangePassRequest, BindingResult result) {
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        UserInfo currentUser = userService.getCurrentAuthenticatedUser();
        if (!currentUser.getIs_delete() && (currentUser.getRole().contains("1"))) {
            return userService.changePass(userDTOChangePassRequest);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
      //  return userService.changePass(userDTOChangePassRequest);
    }

    //======================================== search user================================
    @GetMapping("/roleAdmin/getAllUser")
    public ResponseEntity<?> getAllUsers(){
        return userService.getAllUser();
    }

    //======================================== get user================================
    @GetMapping("/id")
    ResponseEntity<?> searchUserId(@RequestParam String idUser){
        return userService.getUserById(idUser);
    }

    @GetMapping("/getUsernameByEmail")
    public ResponseEntity<?> getUsernameByEmail(@RequestParam @Email(message = "Invalid email format") String email) {
        return userService.getUserByEMail(email);
    }

    //======================================== delete ================================
    @DeleteMapping("roleAdmin/{id}")
    ResponseEntity<?> deleteUser(@PathVariable String id){
        return userService.deleteUser(id);
    }

    //======================================== Refresh token ================================
    @PostMapping(path = "refreshToken")
    public JwtResponse refreshToken(@RequestBody RefreshTokenRequest refreshTokenRequest) {
        return refreshTokenService.findByToken(refreshTokenRequest.getToken())
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(userInfo -> {
                    String accessToken = jwtService.generateToken(userInfo.getUserName(), userInfo.getRole());
                    return JwtResponse.builder()
                            .accessToken(accessToken)
                            .token(refreshTokenRequest.getToken())
                            .expirationTime(new Date(System.currentTimeMillis()+1000*60*2).toInstant())
                            .build();
                }).orElseThrow(() -> new RuntimeException(
                        "Refresh token is not in database!"));
    }
}
