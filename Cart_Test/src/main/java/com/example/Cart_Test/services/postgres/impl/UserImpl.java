package com.example.Cart_Test.services.postgres.impl;

import com.example.Cart_Test.dto.request.UserDTOChangePassRequest;
import com.example.Cart_Test.dto.request.UserDTOInfoRequest;
import com.example.Cart_Test.dto.request.UserDtoLoginRequest;
import com.example.Cart_Test.dto.request.UserDtoSignUpRequest;
import com.example.Cart_Test.dto.response.ObjectResponse;
import com.example.Cart_Test.dto.response.UserDTOInfoResponse;
import com.example.Cart_Test.dto.response.UserDTOTokenResponse;
import com.example.Cart_Test.entity.postgres.RefreshToken;
import com.example.Cart_Test.entity.postgres.UserInfo;
import com.example.Cart_Test.model.UserInfoUserDetails;
import com.example.Cart_Test.model.UserMapper;
import com.example.Cart_Test.repositories.postgres.UserRepositories;
import com.example.Cart_Test.services.postgres.UserService;
import com.example.Cart_Test.util.IDGenerator;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.UserPrincipal;
import java.util.*;

@Service
@Transactional
public class UserImpl implements UserService {
    private static final Path CURRENT_FOLDER = Paths.get(System.getProperty("user.dir"));
    private Logger logger = LoggerFactory.getLogger(UserImpl.class);
    @Autowired
    private UserRepositories userRepositories;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RefreshTokenService refreshTokenService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private ModelMapper modelMapper;
    private Long loginTimeMillis;

    @Override
    public UserInfo isExitUser(String id) {
        if(userRepositories.findById(id).isPresent()){
            return userRepositories.findById(id).get();
        }
        return null;
    }

    @Override
    public UserInfo getCurrentAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.getPrincipal() instanceof UserInfoUserDetails) {
            // Lấy thông tin từ UserPrincipal nếu đang sử dụng Custom UserDetailsService
            UserInfoUserDetails userPrincipal = (UserInfoUserDetails) authentication.getPrincipal();
            return userPrincipal.getUserInfo();
        } else {
            // Xử lý logic nếu không thể lấy được thông tin người dùng
            throw new RuntimeException("Cannot get current authenticated user");
        }
    }

    @Override
    public ResponseEntity<?> signUpUser(UserDtoSignUpRequest userDto) {
        try {
            String s = validateAccount(userDto.getUserName(), userDto.getPhoneNumber(), userDto.getEmail());
            if (s != null) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(
                        new ObjectResponse(false, s, "")
                );
            }
            UserInfo user = setUser(userDto);
            List<String> l = new ArrayList<>();
            l.add("0");
            user.setRole(l);
            UserInfo u = new UserInfo();
            try {
                u = userRepositories.save(user);
            } catch (ConstraintViolationException e) {
                Map<String, String> errors = new HashMap<>();
                e.getConstraintViolations().forEach(violation -> {
                    String propertyPath = violation.getPropertyPath().toString();
                    String message = violation.getMessage();
                    errors.put(propertyPath, message);
                });
                return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
            }
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "SignUp Success", modelMapper.map(u,UserDTOInfoResponse.class))
            );
        } catch (Exception e) {
            logger.error("Error updating user: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> signupByAdmin(UserDtoSignUpRequest userDto) {
        try {
            String s = validateAccount(userDto.getUserName(), userDto.getPhoneNumber(), userDto.getEmail());
            if (s != null) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(
                        new ObjectResponse(false, s, "")
                );
            }
            UserInfo user = setUser(userDto);
            List<String> l = new ArrayList<>();
            l.add("1");
            user.setRole(l);
            UserInfo u = userRepositories.save(user);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "SignUp Success", modelMapper.map(u, UserDTOInfoResponse.class))
            );
        } catch (Exception e) {
            logger.error("Error updating user: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> loginUser(UserDtoLoginRequest loginDto) {
        String msg = "";
        UserInfo user1 = userRepositories.findByUserName(loginDto.getUsername());
        UserDTOTokenResponse loginUserDTO = new UserDTOTokenResponse();
        if (user1 != null) {
            if(user1.getIs_delete()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Your account has been deleted", "")
                );
            }
            String password = loginDto.getPassword();
            String encodedPassword = user1.getPassword();
            Boolean isPwdRight = passwordEncoder.matches(password, encodedPassword);
            if (isPwdRight) {
                Optional<UserInfo> employee = userRepositories.findOneByUserNameAndPassword(loginDto.getUsername(), encodedPassword);
                if (employee.isPresent()) {
                    System.out.println("employee " + employee.toString());
                    Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
                    System.out.println("employee " + employee.toString());
                    if (authentication.isAuthenticated()){
                        RefreshToken refreshToken = refreshTokenService.createRefreshToken(loginDto.getUsername());
                        loginUserDTO.setAccessToken(jwtService.generateToken(loginDto.getUsername(), user1.getRole()));
                        loginUserDTO.setExpirationTime(new Date(System.currentTimeMillis()+1000*60*2).toInstant());
                        loginUserDTO.setToken(refreshToken.getToken());
                        loginUserDTO.setIdUser(user1.getIdUser());
                        loginUserDTO.setRoles(user1.getRole());
                        System.out.println("employee " + loginUserDTO.toString());
                    }
                    return ResponseEntity.status(HttpStatus.OK).body(
                            new ObjectResponse(true, "Login successfully", loginUserDTO)
                    );
                } else {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                            new ObjectResponse(false, "Login failed", "")
                    );
                }
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Invalid username or password.", "")
                );
            }
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(false, "Account not found.", "")
            );
        }
    }

    @Override
    public ResponseEntity<?> updateUser(UserDTOInfoRequest u) {
        logger.info("id user " + u.getIdUser());
        try {
            Optional<UserInfo> userInfoOptional = userRepositories.findById(u.getIdUser());
            // The account does not exist
            if (!userInfoOptional.isPresent())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "User not found", "")
                );

            // validate
            String s = validateAccount(u.getUserName(), u.getPhoneNumber(), u.getEmail());
            if (s != null)
                return ResponseEntity.status(HttpStatus.CONFLICT).body(
                        new ObjectResponse(false, s, "")
                );

            UserInfo userInfo = setUpdateUserInfo(u, userInfoOptional.get());
            // account was deleted
            if (userInfo.getIs_delete())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Your account has been deleted", "")
                );
            // update success
            UserInfo updatedUser = userRepositories.save(userInfo);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "User updated successfully", modelMapper.map(updatedUser, UserDTOInfoResponse.class))
            );

        } catch (Exception e) {
            logger.error("Error updating user: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> changePass(UserDTOChangePassRequest request) {
        logger.info("=========================Attempting to change email with email: " +  request.getEmail());
        try {
            UserInfo user = userRepositories.findByEmail(request.getEmail() );

            // account not found
            if (user == null)
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ObjectResponse(false, "User not found for email: " + request.getEmail(), "")
                );

            // account was deleted
            if(user.getIs_delete())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Your account has been deleted", "")
                );

            // change success
            logger.info("before changing your password : " +  user.getPassword());
            boolean isPwdRight = passwordEncoder.matches(request.getCurrentPassword(), user.getPassword());
            if(!isPwdRight)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Incorrect current password", "")
                );
            user.setPassword(this.passwordEncoder.encode(request.getNewPassword()));
            logger.info("after changing your password : " +  user.getPassword());
            userRepositories.save(user);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "Password changed successfully", null)
            );
        } catch (DataIntegrityViolationException e) {
            logger.error("Data integrity violation while updating user: ", e);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(
                    new ObjectResponse(false, "Conflict error: " + e.getMessage(), e.getMostSpecificCause().getMessage())
            );
        } catch (Exception e) {
            logger.error("Error updating user: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> getAllUser() {
        try {
            List<UserInfo> userInfos = userRepositories.findAll();
            List<UserInfo> list = new ArrayList<>();

            // Filter deleted accounts
            for(UserInfo u: userInfos){
                if(!u.getIs_delete()){
                    list.add(u);
                }
            }
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "", userMapper.toDto(list))
            );
        } catch (Exception e) {
            logger.error("Error updating user: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> getUserById(String idUser) {
        try {

            // account not found
            UserInfo userInfo = userRepositories.findById(idUser)
                    .orElseThrow(() -> new RuntimeException("User with id " + idUser + " not found"));

            // account has been deleted
            if(userInfo.getIs_delete())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Your account has been deleted", "")
                );

            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "", modelMapper.map(userInfo, UserDTOInfoResponse.class))
            );
        } catch (Exception e) {
            logger.error("Error updating user: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> getUserByEMail(String email) {
        try {
            UserInfo user = userRepositories.findByEmail(email);

            // account not found
            if(user == null)
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ObjectResponse(false, "User not found for email: " + email, "")
                );

            // account has been deleted
            if(user.getIs_delete()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Your account has been deleted", "")
                );
            }
            return ResponseEntity.status(HttpStatus.OK).body(user.getUserName());

        } catch (Exception e) {
            logger.error("Error updating user: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> deleteUser(String idUser) {
        logger.info("Attempting to delete user with id: " + idUser);

        Optional<UserInfo> userInfoOptional = userRepositories.findById(idUser);

        // account not found
        if (!userInfoOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(false, "User with id " + idUser + " not found", "")
            );
        }

        // account has been deleted
        UserInfo userInfo = userInfoOptional.get();
        if (userInfo.getIs_delete()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(false, "Your account has already been deleted", "")
            );
        }

        // Accounts that have not logged in are completely deleted
        try {
            userRepositories.delete(userInfo);
            Optional<UserInfo> userInfoOptional1 = userRepositories.findById(idUser);
            if (!userInfoOptional1.isPresent()) {
                logger.info("User with id {} successfully deleted", idUser);
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ObjectResponse(true, "User deleted successfully", "")
                );
            }
        } catch (Exception e) {
            // Ensure the user is marked as deleted even if delete operation fails
            userInfo.setIs_delete(Boolean.TRUE);
            userRepositories.save(userInfo);

            Optional<UserInfo> updatedUserInfoOptional = userRepositories.findById(idUser);
            if (!updatedUserInfoOptional.isPresent() || Boolean.TRUE.equals(updatedUserInfoOptional.get().getIs_delete())) {
                logger.info("User with id {} successfully deleted", idUser);
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ObjectResponse(true, "User deleted successfully", "")
                );
            }
        }
        logger.error("Failed to delete user with id " + idUser);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                new ObjectResponse(false, "Failed to delete user", "")
        );
    }

    public int getID(){
        int numberOfExisting = userRepositories.countAllUsers();
        if(numberOfExisting == 0){
            numberOfExisting = 1;
        }else {
            UserInfo last = userRepositories.findTopByOrderByIdUserDesc();

            String lastId = last != null ? last.getIdUser() : "US0";
            int lastNumber = Integer.parseInt(lastId.replace("US", ""));

            int newNumber = lastNumber + 1;
            numberOfExisting = newNumber;
        }
        return numberOfExisting;
    }

    public String validateAccount(String us, String phone, String email){
        UserInfo foundUser = userRepositories.findByUserName(us.trim());
        UserInfo foundPhone = userRepositories.findByPhoneNumber(phone.trim());
        UserInfo foundEmail = userRepositories.findByEmail(email.trim());
        if (foundUser != null || foundPhone != null || foundEmail != null) {
            return "Account name, phone number, or email that already exists";
        }
        return null;
    }

    public UserInfo setUser(UserDtoSignUpRequest userDto){
        IDGenerator idGenerator = new IDGenerator("US", getID());
        UserInfo user = new UserInfo(idGenerator);
        user.setUserName(userDto.getUserName());
        user.setPassword(this.passwordEncoder.encode(userDto.getPassword()));
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setAddress(userDto.getAddress());
        user.setGender(userDto.getGender());
        return user;
    }

    private UserInfo setUpdateUserInfo(UserDTOInfoRequest u, UserInfo userInfo) {
        if (u.getUserName() != null) {userInfo.setUserName(u.getUserName());}
        if (u.getEmail() != null) {userInfo.setEmail(u.getEmail()); }
        if (u.getPhoneNumber() != null) {userInfo.setPhoneNumber(u.getPhoneNumber());}
        if (u.getAddress() != null) { userInfo.setAddress(u.getAddress()); }
        if (u.getGender() != null) {userInfo.setGender(u.getGender());}
        if (u.getAvatar() != null) {userInfo.setGender(u.getAvatar());}
        return userInfo;
    }
    private void saveImage(MultipartFile avatar, UserDTOInfoRequest u, UserInfo userInfo) throws IOException {
        String originalFilename = avatar.getOriginalFilename();

        System.out.println("Avatar: "+ originalFilename);
        String extension = originalFilename.substring(originalFilename.lastIndexOf('.'));
        String newFilename = u.getIdUser() + "_avatar_" + System.currentTimeMillis() + extension;

        String currentImage = userInfo.getAvatar();
        if (currentImage != null && !currentImage.isEmpty()) {
            Path imagePath = Paths.get(currentImage);
            Files.deleteIfExists(imagePath);
        }

        Path staticPath = Paths.get("static");
        Path imagePath = Paths.get("images");
        if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))) {
            Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
        }

        Path file = CURRENT_FOLDER.resolve(staticPath)
                .resolve(imagePath).resolve(newFilename);

        try (OutputStream os = Files.newOutputStream(file)) {
            os.write(avatar.getBytes());
        }
        userInfo.setAvatar(imagePath.resolve(newFilename).toString());
    }
}

