package com.example.Cart_Test.services.postgres.impl;


import com.example.Cart_Test.entity.postgres.UserInfo;
import com.example.Cart_Test.model.UserInfoUserDetails;
import com.example.Cart_Test.repositories.postgres.UserRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@Transactional
public class UserInfoUserDetailsService implements UserDetailsService {

    @Autowired

    private UserRepositories repositories;

    @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfo user = repositories.findByUserName(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found: " + username);
        }

        return new UserInfoUserDetails(user);
    }

}