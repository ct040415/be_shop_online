package com.example.Cart_Test.services.postgres.impl;

import com.example.Cart_Test.entity.postgres.RefreshToken;
import com.example.Cart_Test.entity.postgres.UserInfo;
import com.example.Cart_Test.repositories.postgres.RefreshTokenRepository;
import com.example.Cart_Test.repositories.postgres.UserRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Service
public class RefreshTokenService {

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;
    @Autowired
    //private UserInfoRepository userInfoRepository;
    private UserRepositories repositories;
    @Autowired
    private UserRepositories customerRepositories;

    public RefreshToken createRefreshToken(String username) {
        UserInfo user = repositories.findByUserName(username);
        if (user == null) {
            throw new IllegalArgumentException("User not found with username: " + username);
        }

        Optional<RefreshToken> existingToken = refreshTokenRepository.findByUser(user);
        if (existingToken.isPresent()) {
            RefreshToken refreshToken = existingToken.get();
            refreshToken.setToken(UUID.randomUUID().toString());
            refreshToken.setExpiryDate(Instant.now().plus(Duration.ofDays(1)));
            return refreshTokenRepository.save(refreshToken);
        } else {
            RefreshToken refreshToken = RefreshToken.builder()
                    .user(user)
                    .token(UUID.randomUUID().toString())
                    .expiryDate(Instant.now().plus(Duration.ofDays(1)))
                    .build();
            return refreshTokenRepository.save(refreshToken);
        }
    }

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }


    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepository.delete(token);
            throw new RuntimeException(token.getToken() + " Refresh token was expired. Please make a new signin request");
        }
        return token;
    }

}