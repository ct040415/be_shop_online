package com.example.Cart_Test.services.postgres;


import com.example.Cart_Test.entity.postgres.TokenDevice;

public interface TokenDeviceService {

    void saveTokenDevice(TokenDevice tokenDevice);
    String getTokenByUserId(String idUser);
}
