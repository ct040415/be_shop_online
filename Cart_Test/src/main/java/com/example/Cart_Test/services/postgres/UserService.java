package com.example.Cart_Test.services.postgres;

import com.example.Cart_Test.dto.request.UserDTOChangePassRequest;
import com.example.Cart_Test.dto.request.UserDTOInfoRequest;
import com.example.Cart_Test.dto.request.UserDtoLoginRequest;
import com.example.Cart_Test.dto.request.UserDtoSignUpRequest;
import com.example.Cart_Test.entity.postgres.UserInfo;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;


public interface UserService {

    UserInfo isExitUser(String id);
    UserInfo getCurrentAuthenticatedUser();
    ResponseEntity<?> signUpUser(@Valid UserDtoSignUpRequest userDto);
    ResponseEntity<?> signupByAdmin(@Valid UserDtoSignUpRequest userDtoSignInRequest);
    ResponseEntity<?> loginUser(@Valid UserDtoLoginRequest loginDto);
    ResponseEntity<?> updateUser(@Valid UserDTOInfoRequest userDTOInfoRequest) ;
    ResponseEntity<?> changePass(@Valid UserDTOChangePassRequest userDTOChangePassRequest);
    ResponseEntity<?> getAllUser();
    ResponseEntity<?> getUserById(String idUser);
    ResponseEntity<?> getUserByEMail(@Valid String email);
    ResponseEntity<?>deleteUser(String idUser);
}
