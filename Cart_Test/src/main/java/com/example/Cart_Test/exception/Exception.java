package com.example.Cart_Test.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Exception extends RuntimeException {
    private int code;
    private String message;
}
