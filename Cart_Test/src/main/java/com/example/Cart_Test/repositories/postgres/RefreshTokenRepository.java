package com.example.Cart_Test.repositories.postgres;

import com.example.Cart_Test.entity.postgres.RefreshToken;
import com.example.Cart_Test.entity.postgres.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
public interface RefreshTokenRepository extends JpaRepository<RefreshToken,Integer> {
    Optional<RefreshToken> findByToken(String token);

    Optional<RefreshToken> findByUser(UserInfo user);
}