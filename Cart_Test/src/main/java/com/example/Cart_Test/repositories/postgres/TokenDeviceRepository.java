package com.example.Cart_Test.repositories.postgres;


import com.example.Cart_Test.entity.postgres.TokenDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenDeviceRepository extends JpaRepository<TokenDevice, String> {
    TokenDevice findByIdUser(String idUser);

}
