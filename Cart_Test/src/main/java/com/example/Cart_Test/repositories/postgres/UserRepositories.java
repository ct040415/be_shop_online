package com.example.Cart_Test.repositories.postgres;

import com.example.Cart_Test.entity.postgres.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepositories extends JpaRepository<UserInfo, String>, JpaSpecificationExecutor<UserInfo> {
    Optional<UserInfo> findOneByEmailAndPassword(String email, String password);
    UserInfo findByUserName(String userName);

    UserInfo findByEmail(String email);

    UserInfo findByPhoneNumber(String phoneNumber);

    @Query("SELECT COUNT(u) FROM UserInfo u")
    int countAllUsers();

    List<UserInfo> findByUserNameContainingIgnoreCase(String userName);

    Optional<UserInfo> findOneByUserNameAndPassword(String username, String encodedPassword);

    UserInfo findTopByOrderByIdUserDesc();

    Optional<UserInfo> findByIdUser(String idUser);

//
//    List<User> findByphoneNumber(String phoneNumber);
}
