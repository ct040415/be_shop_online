package com.example.Cart_Test.entity.postgres;

import com.example.Cart_Test.util.Constants;
import com.example.Cart_Test.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idUser")
@Table(name = "users")
public class UserInfo {
    @Id
    @Column(name = "idUser")
    private String idUser;

    @Size(max = Constants.NAME_MAX_LENGTH, min = Constants.NAME_MIN_LENGTH)
    @Column(unique = true, columnDefinition = "varchar(1000)")
    private String userName;

    @Size(max = Constants.PASSWORD_MAX_LENGTH, min = Constants.PASSWORD_MIN_LENGTH)
    private String password;

    @Column(columnDefinition = "varchar(1000)")
    @Size(max = Constants.EMAIL_MAX_LENGTH)
    @Pattern(regexp = Constants.PATTERN_EMAIL)
    private String email;

    @Pattern(regexp = Constants.PATTERN_PHONENUMBER)
    private String phoneNumber;

    @Column(columnDefinition = "varchar(1000)")
    private String address;

    @ElementCollection
    @CollectionTable(name = "list_role", joinColumns = @JoinColumn(name = "role_id"))
    @Column(name = "role")
    private List<String> role;

    private Boolean is_delete = false;

    @Column(columnDefinition = "varchar(1000)")
    private String gender;

    @Column(columnDefinition = "varchar(1000)")
    private String avatar;

    @Column(columnDefinition = "varchar(1000)")
    private String background;

    public UserInfo(IDGenerator idGenerator){
        this.idUser = idGenerator.generateNextID();
    }
}
