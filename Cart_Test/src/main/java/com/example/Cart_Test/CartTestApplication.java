package com.example.Cart_Test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartTestApplication.class, args);
	}

}
