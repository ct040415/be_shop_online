package com.api19_4.api19_4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class  Api194Application {
    public static void main(String[] args) {
        SpringApplication.run(Api194Application.class, args);
    }
}
