package com.api19_4.api19_4.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Exception extends RuntimeException {
    private int code;
    private String message;
}
