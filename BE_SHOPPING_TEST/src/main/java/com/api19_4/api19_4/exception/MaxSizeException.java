package com.api19_4.api19_4.exception;

import com.api19_4.api19_4.dto.response.ObjectResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@ControllerAdvice
public class MaxSizeException {
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<ObjectResponse> handleMaxSizeException(MaxUploadSizeExceededException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ObjectResponse(false, "File size exceeds maximum permitted size of 1MB.", "")
        );
    }
}
