package com.api19_4.api19_4.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.stream.Collectors;

public class VNPayUtils {
    public static boolean validateSignature(Map<String, String> params, String secretKey) {
        String receivedHash = params.remove("vnp_SecureHash");

        String data = params.entrySet().stream()
                .filter(entry -> !entry.getKey().equals("vnp_SecureHashType"))
                .sorted(Map.Entry.comparingByKey())
                .map(entry -> entry.getKey() + "=" + entry.getValue())
                .collect(Collectors.joining("&"));

        String calculatedHash = hmacSHA512(secretKey, data);

        return receivedHash != null && receivedHash.equalsIgnoreCase(calculatedHash);
    }

    private static String hmacSHA512(String key, String data) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(key.getBytes(StandardCharsets.UTF_8));
            byte[] bytes = md.digest(data.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (byte b : bytes) {
                sb.append(String.format("%02x", b));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("HMAC SHA-512 algorithm not found", e);
        }
    }
}
