package com.api19_4.api19_4.dto;

import com.api19_4.api19_4.enums.SenderTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ChatBotDTO {
    private Long idChatBot;
    private String content;
    private String image;
    private SenderTypeEnum sender;
    private String timestamp;
    private String time;
    private String url;
    private String productName;
    private String brand;
    private int size;
    private int priceFrom;
    private int priceTo;
    private String categoryName;
    private boolean coupons;
    private boolean display;
}
