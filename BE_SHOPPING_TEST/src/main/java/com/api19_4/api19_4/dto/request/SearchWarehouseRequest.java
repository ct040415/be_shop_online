package com.api19_4.api19_4.dto.request;

import com.api19_4.api19_4.entity.entityPostgres.Warehouse;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


@Setter
@Getter
public class SearchWarehouseRequest implements Specification<Warehouse> {
    private String name;
    private String address;
    private String information;

    @Override
    public Specification<Warehouse> and(Specification<Warehouse> other) {
        return Specification.super.and(other);
    }

    @Override
    public Specification<Warehouse> or(Specification<Warehouse> other) {
        return Specification.super.or(other);
    }

    @Override
    public Predicate toPredicate(Root<Warehouse> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return null;
    }
}
