package com.api19_4.api19_4.dto.request;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BillItemDTORequest {

    private String idBillItem;

    private String idProd;

    private int quantity;

    private int size;

    private double totalPriceProd;

}
