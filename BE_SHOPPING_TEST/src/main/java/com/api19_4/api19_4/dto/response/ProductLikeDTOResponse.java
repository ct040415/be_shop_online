package com.api19_4.api19_4.dto.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductLikeDTOResponse {

    private String idProd;

    private String idUser;

    private String productName;

    private double retailPrice;

    private String unitName;

    private String description;

    private String brand;

    private int quantitySold;

    private int quantityInAvailable;

    private int coupons;
}
