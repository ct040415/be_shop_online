package com.api19_4.api19_4.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
public class BatchDTORequest {

    private String idBatch;

    private String name;

    private LocalDateTime dateImport;

    private LocalDateTime manufacturingDate;

    private LocalDateTime expirationDate;

    private List<BatchItemDTORequest> batchItemDTORequests;

    private String idWarehouse;

    private String idSupplier;

    private double payableAmount;

    private double paidAmount;
}
