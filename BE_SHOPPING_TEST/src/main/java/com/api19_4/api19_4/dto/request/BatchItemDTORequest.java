package com.api19_4.api19_4.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
@Setter
@Getter
public class BatchItemDTORequest {
    private String idBatchItem;

    private String idBatch;

    private String idProduct;

    private String productType;

    private int totalQuantity;

    private Integer discount;

    private int size;

    private String note;

    private String status;

}
