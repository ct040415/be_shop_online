package com.api19_4.api19_4.dto.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BillItemDTOResponse {
    private String idProd;

    private String productName;

    private double retailPrice;

    private String unitName;

    private String description;

    private String brand;

    private int quantity;

    private double totalPriceProd;

    private int quantitySold;

    private int quantityInAvailable;

    private int coupons;
}
