package com.api19_4.api19_4.dto.response;

import com.api19_4.api19_4.entity.entityPostgres.BatchItem;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
public class BatchDTOResponse {

    private String idBatch;

    private String name;

    private LocalDateTime dateImport;

    private LocalDateTime manufacturingDate;

    private LocalDateTime expirationDate;

    private List<BatchItem> batchItems;

    private String idWarehouse;

    private String idSupplier;

}
