package com.api19_4.api19_4.dto.request;

import com.api19_4.api19_4.util.Constants;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Setter
@Getter
public class CreateBatchItemDTORequest {

    @NotBlank(message = "idProduct cannot be empty")
    private String idProduct;

    @NotBlank(message = "ProductType cannot be empty")
    private String productType;

    @NotBlank(message = "PurchaseUnitPrice cannot be empty")
    private double purchaseUnitPrice;

    @NotBlank(message = "RetailUnitPrice cannot be empty")
    private double retailUnitPrice;

    @NotBlank(message = "TotalQuantity cannot be empty")
    private int totalQuantity;

    @NotBlank(message = "TotalPurchasePrice cannot be empty")
    private double totalPurchasePrice;

    private Integer discount;

    @NotBlank(message = "Color cannot be empty")
    private String color;

    @NotBlank(message = "Size cannot be empty")
    @Size(max = Constants.SHOE_SIZE_MAX, min = Constants.SHOE_SIZE_MIN, message = "Size must be between {min} and {max}")
    private int size;

    private String note;

    private String status;
}
