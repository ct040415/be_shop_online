package com.api19_4.api19_4.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
@Setter
@Getter
public class BillDTOResponse {

    private String idBill;

    private String numberPhoneCustomer;

    private String addressCustomer;

    private LocalDateTime dateTimeOrder;

    private String status;

    private double totalPayment;

    private double payableAmount;

    private double shippingFee;

    private String note;

    private List<BillItemDTOResponse> billItemDTOResponses;

    private String idUser;
}

