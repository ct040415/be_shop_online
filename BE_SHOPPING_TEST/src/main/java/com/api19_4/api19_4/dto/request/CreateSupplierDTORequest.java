package com.api19_4.api19_4.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CreateSupplierDTORequest {
    @NotBlank(message = "Supplier name cannot be empty.")
    private String name;

    @NotBlank(message = "Supplier address cannot be empty")
    private String address;

    @NotBlank(message = "Supplier infor cannot be empty")
    private String information;
}
