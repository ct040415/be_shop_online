package com.api19_4.api19_4.dto.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class WarehouseDTOResponse {
    private String idWarehouse;
    private String name;
    private String address;
    private String information;
}
