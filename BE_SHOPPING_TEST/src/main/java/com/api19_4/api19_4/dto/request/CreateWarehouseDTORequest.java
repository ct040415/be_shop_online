package com.api19_4.api19_4.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CreateWarehouseDTORequest {
    @NotBlank(message = "Warehouse name cannot be empty.")
    private String name;

    @NotBlank(message = "Warehouse address cannot be empty")
    private String address;

    @NotBlank(message = "Warehouse information cannot be empty")
    private String information;
}
