package com.api19_4.api19_4.dto.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDTOInfoResponse {

    private String idUser;

    private String userName;

    private String email;

    private String phoneNumber;

    private String address;

    private String gender;
}
