package com.api19_4.api19_4.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.List;
@Setter
@Getter
public class CreateBatchDTORequest {

    @NotBlank(message = "Name cannot be empty")
    private String name;

    @NotBlank(message = "OrderInitiator cannot be empty")
    private String orderInitiator;

    private LocalDateTime dateImport;

    private LocalDateTime manufacturingDate;

    private LocalDateTime expirationDate;

    @NotEmpty(message = "BatchItem cannot be empty")
    private List<CreateBatchItemDTORequest> batchItemDTORequests;

    @NotBlank(message = "idWarehouse cannot be empty")
    private String idWarehouse;

    @NotBlank(message = "idSupplier cannot be empty")
    private String idSupplier;

    private double payableAmount;

    private double paidAmount;
}
