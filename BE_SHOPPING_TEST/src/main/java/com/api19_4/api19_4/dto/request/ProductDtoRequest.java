package com.api19_4.api19_4.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DecimalFormat;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDtoRequest {

    private String idProd;

    private String productName;

    private double retailPrice;

    private String unitName;

    private String description;

    private String brand;

    private int quantityInStock;

    private int quantitySold;

    private int quantityInAvailable;

    private int quantityOrdered;

    private int coupons;

    private String idSupplier;

    private List<String> idWarehouses;

    private List<String> image;

    private String productType;

    public String getFormattedDiscountedPrice() {
        double discount = (100 - coupons) / 100.0; // Tính phần trăm giảm giá
        double discountedPrice = retailPrice * discount; // Giá sau khi áp dụng khuyến mãi

        DecimalFormat decimalFormat = new DecimalFormat("#,###"); // Định dạng format tiền tệ
        return decimalFormat.format(discountedPrice); // Trả về giá sau khi khuyến mãi đã được định dạng tiền tệ
    }
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#,###")
    public String getFormattedPrice() {
        DecimalFormat decimalFormat = new DecimalFormat("#,###");
        return decimalFormat.format(retailPrice);
    }

}
