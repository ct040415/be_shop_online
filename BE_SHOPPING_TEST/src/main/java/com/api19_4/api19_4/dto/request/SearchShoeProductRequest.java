package com.api19_4.api19_4.dto.request;


import com.api19_4.api19_4.entity.entityPostgres.ShoeProduct;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


@Getter
@Setter
public class SearchShoeProductRequest implements Specification<ShoeProduct> {
    private String productName;
    private String brand;
    private int size;
    private float priceFrom;
    private float priceTo;
    private String productType;
    private  boolean coupons;
    private String color;

    @Override
    public Specification<ShoeProduct> and(Specification<ShoeProduct> other) {
        return Specification.super.and(other);
    }

    @Override
    public Specification<ShoeProduct> or(Specification<ShoeProduct> other) {
        return Specification.super.or(other);
    }

    @Override
    public Predicate toPredicate(Root<ShoeProduct> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return null;
    }
}
