package com.api19_4.api19_4.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ShoeProductDtoRequest extends ProductDtoRequest{

    private List<String> colors;

}
