package com.api19_4.api19_4.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
@Setter
@Getter
public class BillDTORequest {

    private String idBill;

    private String numberPhoneCustomer;

    private String addressCustomer;

    private LocalDateTime dateTimeOrder;

    private String status;

    private double totalPayment;

    private double payableAmount;

    private double shippingFee;

    private String note;

    private List<BillItemDTORequest> billItemDTORequests;

    private String idUser;

}
