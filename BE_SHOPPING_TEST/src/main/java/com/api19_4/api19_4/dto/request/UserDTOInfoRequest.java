package com.api19_4.api19_4.dto.request;

import com.api19_4.api19_4.util.Constants;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Setter
@Getter
public class UserDTOInfoRequest {

    private String idUser;

    private String userName;

    @Email(message = "Invalid email format")
    @Size(max = Constants.EMAIL_MAX_LENGTH, message = "Email must not exceed {max} characters")
    private String email;

    @Pattern(regexp = Constants.PATTERN_PHONENUMBER, message = "Invalid phone number format")
    @Size(min = Constants.PHONENUMBER_MIN_LENGTH, max = Constants.PHONENUMBER_MAX_LENGTH, message = "Phone number must be between {min} and {max} characters")
    private String phoneNumber;

    private String address;

    private String gender;

    private String avatar;
}
