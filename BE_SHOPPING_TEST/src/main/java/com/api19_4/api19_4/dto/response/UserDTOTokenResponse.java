package com.api19_4.api19_4.dto.response;

import lombok.*;

import java.time.Instant;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserDTOTokenResponse {
    private String idUser;
    private String roles;
    private String accessToken;
    private String token;
    private Instant expirationTime;
}
