package com.api19_4.api19_4.dto.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SupplierDTORequest {
    private String idSupplier;
    private String name;
    private String information;
    private String address;
}
