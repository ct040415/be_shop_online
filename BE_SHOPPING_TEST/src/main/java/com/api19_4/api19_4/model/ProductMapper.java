package com.api19_4.api19_4.model;

import com.api19_4.api19_4.dto.response.ProductDTOResponse;
import com.api19_4.api19_4.entity.entityPostgres.Product;
import com.api19_4.api19_4.entity.entityPostgres.Warehouse;
import com.api19_4.api19_4.services.servicePostgres.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductMapper {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProductService productService;
    public List<ProductDTOResponse> toDto(List<Product> products) {
        return  products.stream()
                .map(product -> modelMapper.map(product, ProductDTOResponse.class))
                .collect(Collectors.toList());
    }


}
