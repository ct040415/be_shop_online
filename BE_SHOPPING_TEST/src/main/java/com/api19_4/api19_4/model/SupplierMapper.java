package com.api19_4.api19_4.model;

import com.api19_4.api19_4.dto.request.SupplierDTORequest;
import com.api19_4.api19_4.entity.entityPostgres.Supplier;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SupplierMapper {
    @Autowired
    private ModelMapper modelMapper;
    public List<SupplierDTORequest> toDto(List<Supplier> suppliers){
        return  suppliers.stream()
                .map(supplier -> modelMapper.map(supplier, SupplierDTORequest.class))
                .collect(Collectors.toList());
    }
}
