package com.api19_4.api19_4.model;

import com.api19_4.api19_4.dto.response.BatchDTOResponse;
import com.api19_4.api19_4.entity.entityPostgres.Batch;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BatchMapper {
    @Autowired
    private ModelMapper modelMapper;
    public List<BatchDTOResponse> toDto(List<Batch> batches){
        return  batches.stream()
                .map(batch -> modelMapper.map(batch, BatchDTOResponse.class))
                .collect(Collectors.toList());
    }


}
