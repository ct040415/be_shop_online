package com.api19_4.api19_4.model;

import com.api19_4.api19_4.dto.response.UserDTOInfoResponse;
import com.api19_4.api19_4.entity.entityPostgres.UserInfo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {
    @Autowired
    private ModelMapper modelMapper;
    public List<UserDTOInfoResponse> toDto(List<UserInfo> userInfos){
        return  userInfos.stream()
                .map(userInfo -> modelMapper.map(userInfo, UserDTOInfoResponse.class))
                .collect(Collectors.toList());
    }
}
