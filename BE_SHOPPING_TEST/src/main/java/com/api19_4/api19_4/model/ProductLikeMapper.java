//package com.api19_4.api19_4.model;
//
//import com.api19_4.api19_4.dto.response.ProductLikeDTOResponse;
//import com.api19_4.api19_4.entity.entitySqlserver.ProductLike;
//import org.modelmapper.ModelMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//import java.util.stream.Collectors;
//@Component
//public class ProductLikeMapper {
//    @Autowired
//    private ModelMapper modelMapper;
//    public List<ProductLikeDTOResponse> toDto(List<ProductLike> productLikes){
//        return  productLikes.stream()
//                .map(productLike -> modelMapper.map(productLike, ProductLikeDTOResponse.class))
//                .collect(Collectors.toList());
//    }
//}
