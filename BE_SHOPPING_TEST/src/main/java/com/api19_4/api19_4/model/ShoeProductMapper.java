package com.api19_4.api19_4.model;

import com.api19_4.api19_4.dto.response.ShoeProductDtoResponse;
import com.api19_4.api19_4.dto.response.WarehouseDTOResponse;
import com.api19_4.api19_4.entity.entityPostgres.ShoeProduct;
import com.api19_4.api19_4.entity.entityPostgres.Warehouse;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
@Component
public class ShoeProductMapper {
    @Autowired
    private ModelMapper modelMapper;
    public List<ShoeProductDtoResponse> toDto(List<ShoeProduct> shoeProducts) {
        return  shoeProducts.stream()
                .map(shoeProduct -> modelMapper.map(shoeProduct, ShoeProductDtoResponse.class))
                .collect(Collectors.toList());
    }

}
