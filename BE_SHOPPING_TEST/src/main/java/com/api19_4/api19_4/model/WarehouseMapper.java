package com.api19_4.api19_4.model;

import com.api19_4.api19_4.dto.response.WarehouseDTOResponse;
import com.api19_4.api19_4.entity.entityPostgres.Warehouse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
@Component
public class WarehouseMapper {
    @Autowired
    private ModelMapper modelMapper;
    public List<WarehouseDTOResponse> toDto(List<Warehouse> warehouses){
        return  warehouses.stream()
                .map(warehouse -> modelMapper.map(warehouse, WarehouseDTOResponse.class))
                .collect(Collectors.toList());
    }
}
