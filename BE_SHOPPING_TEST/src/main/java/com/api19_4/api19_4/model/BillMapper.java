package com.api19_4.api19_4.model;

import com.api19_4.api19_4.dto.response.BillDTOResponse;
import com.api19_4.api19_4.dto.response.BillItemDTOResponse;
import com.api19_4.api19_4.entity.entityPostgres.Bill;
import com.api19_4.api19_4.entity.entityPostgres.BillItem;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BillMapper {
    @Autowired
    private ModelMapper modelMapper;

    public BillDTOResponse toDto(Bill bill){
        BillDTOResponse billDTOResponse = modelMapper.map(bill, BillDTOResponse.class);
        List<BillItemDTOResponse> billItemDTOResponseList = new ArrayList<>();
        List<BillItem> billItemList = bill.getBillItems();
        for(BillItem billItem : billItemList){
            if(billItem != null){
                BillItemDTOResponse billItemDTOResponse = modelMapper.map(billItem, BillItemDTOResponse.class);
                billItemDTOResponseList.add(billItemDTOResponse);
            }
        }
        billDTOResponse.setBillItemDTOResponses(billItemDTOResponseList);
        return billDTOResponse;
    }

//    public List<BillDTOResponse> toDto(List<Bill> list)

}
