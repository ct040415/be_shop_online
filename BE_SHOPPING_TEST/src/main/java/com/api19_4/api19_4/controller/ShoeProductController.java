package com.api19_4.api19_4.controller;

import com.api19_4.api19_4.dto.request.ShoeProductDtoRequest;
import com.api19_4.api19_4.dto.request.SearchShoeProductRequest;
import com.api19_4.api19_4.dto.response.ObjectResponse;
import com.api19_4.api19_4.dto.response.PageResponse;
import com.api19_4.api19_4.dto.response.ShoeProductDtoResponse;
import com.api19_4.api19_4.services.servicePostgres.ShoeProductService;
import com.api19_4.api19_4.util.SearchUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.io.IOException;
import java.io.InputStream;

@RestController
@RequestMapping(path = "/api/v1/ShoeProducts")
public class ShoeProductController {
    private Logger logger = LoggerFactory.getLogger(ShoeProductController.class);
    @Autowired
    private ShoeProductService shoeProductService;

    //==================================== Thêm sản phẩm  ===========================

    @PostMapping("/create")
    public ResponseEntity<?> createShoeProduct(@RequestBody ShoeProductDtoRequest productDto) {
        try {
            return shoeProductService.createShoeProduct(productDto);
        } catch (Exception e) {
            return new ResponseEntity<>("Error occurred while creating the shoe", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/upload-excel", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> createProductShoeFromExcel( @RequestPart("excelFile") MultipartFile  excelFile) throws IOException {
        if (!isExcelFile(excelFile)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(false, "File format is not supported. Please upload an Excel file.", "")
            );
        }
        try (InputStream is = excelFile.getInputStream()) {
            Workbook workbook = new XSSFWorkbook(is);
            Sheet sheet = workbook.getSheetAt(0); // Giả sử dữ liệu nằm trên sheet đầu tiên
            return shoeProductService.createByExcelShoe(sheet);
        }
        catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Failed to process Excel file.", ex)
            );
        }
    }
    private boolean isExcelFile(MultipartFile file) {
        return file.getOriginalFilename().endsWith(".xlsx");
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateProduct(@RequestBody ShoeProductDtoRequest productDto){
        try {
            return shoeProductService.updateProductShoe(productDto);
        } catch (Exception e) {
            return new ResponseEntity<>("Error occurred while updating the shoe", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{idProd}")
    public ResponseEntity<?> deleteWarehouse(@PathVariable String idProd) {
        try {
            return shoeProductService.deleteProductShoe(idProd);
        } catch (Exception e) {
            return new ResponseEntity<>("Error occurred while deleting the Batch", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/searchAll")
    public ResponseEntity<?> searchAllProduct(){
        return shoeProductService.getAllProductShoe();
    }

    //===========================================Search===========================================

//    @ApiOperation(value = "API tìm kiếm nâng cao")
//    @PostMapping("/roleUser/filter")
//    @ResponseBody
//    public PageResponse<ShoeProductDtoResponse> advanceFilters(@Valid @RequestBody SearchShoeProductRequest searchProductRequest,
//                                                               @PositiveOrZero @RequestParam(required = false, defaultValue = "0") Integer page,
//                                                               @Positive @RequestParam (required = false, defaultValue = "15") Integer size) throws Exception{
//        Pageable pageable = SearchUtil.getPageableFromParamP(page, size);
//        Page<ShoeProductDtoResponse> pageData = shoeProductService.advanceSearchShoe(searchProductRequest, pageable);
//        return new PageResponse<>(pageData);
//    }

    @GetMapping("/roleUser/coupons")
    public ResponseEntity<?> getProductsByCoupons() {
        try {
            return shoeProductService.getAllProductCouponsShoe();
        } catch (Exception e) {
            return new ResponseEntity<>("Error occurred while getting the shoe", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}