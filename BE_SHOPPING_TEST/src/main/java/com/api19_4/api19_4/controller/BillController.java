package com.api19_4.api19_4.controller;
import com.api19_4.api19_4.dto.request.BillDTORequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.api19_4.api19_4.services.servicePostgres.BillService;
import org.springframework.beans.factory.annotation.Autowired;


@RestController
@RequestMapping(path = "/api/v5/Bill")
public class BillController {
    @Autowired
    private BillService billService;

    @PostMapping("/create")
    public ResponseEntity<?> createBill(BillDTORequest request){
        try {
            return billService.createBill(request);
        } catch (Exception e) {
            return new ResponseEntity<>("Error occurred while creating the Bill", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateBill(BillDTORequest request){
        try {
            return billService.updateBill(request);
        } catch (Exception e) {
            return new ResponseEntity<>("Error occurred while updating the Bill", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
