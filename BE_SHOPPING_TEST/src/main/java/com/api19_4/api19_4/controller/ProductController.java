package com.api19_4.api19_4.controller;

import com.api19_4.api19_4.services.servicePostgres.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1/Products")
public class ProductController {
    @Autowired
    ProductService productService;
    private Logger logger = LoggerFactory.getLogger(ProductController.class);
    @GetMapping("/searchAll")
    public ResponseEntity<?> searchAllProduct(){
        return productService.getAllProduct();
    }
}
