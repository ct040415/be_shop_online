package com.api19_4.api19_4.controller;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/v5/InventoryCheck")
@SpringBootApplication
@ComponentScan(basePackages = "com.api19_4.api19_4") // Thay thế bằng package chứa các bean của ứng dụng của bạn
public class InventoryCheckController {

}

