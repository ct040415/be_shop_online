package com.api19_4.api19_4.controller;

import com.api19_4.api19_4.services.servicePostgres.impl.EmailService;
import com.api19_4.api19_4.services.servicePostgres.impl.OTPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;


@Controller
@RestController
@RequestMapping(path = "/api/testOTP")
public class OTPController {
	

@Autowired
public OTPService otpService;
@Autowired
public EmailService emailService;

	@GetMapping("/generateOtp")
	public String generateOTP(@RequestParam("email") String email) throws MessagingException {

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String username = auth.getName();
			int otp = otpService.generateOTP(email);

			emailService.sendOtpMessage(
					"ct040415@gmail.com",
					"OTP - OfficeOrder",
					"Mã OTP của bạn là: "+ otp +
							", " +"mã có thời hạn 2 phút");

			return "success";
		}
	@GetMapping(value ="/validateOtp")
	public String validateOtp(@RequestParam("email") String email,@RequestParam("otpnum") int otpnum){

			String SUCCESS = "Xác minh thành công";
			String FAIL = "OTP không hợp lệ, Vui lòng thử lại!";

			if(otpnum >= 0){
			  int serverOtp = otpService.getOtp(email);
				if(serverOtp > 0){
				  if(otpnum == serverOtp){
					  otpService.clearOTP(email);
					  return SUCCESS;
					}
					else {
						return FAIL;
					   }
				   }else {
				  return FAIL;
				   }
				 }else {
					return FAIL;
			 }
		  }
	}