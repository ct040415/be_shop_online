package com.api19_4.api19_4.controller;

import com.api19_4.api19_4.dto.request.CreateWarehouseDTORequest;
import com.api19_4.api19_4.dto.request.SearchWarehouseRequest;
import com.api19_4.api19_4.dto.request.WarehouseDTORequest;
import com.api19_4.api19_4.dto.response.PageResponse;
import com.api19_4.api19_4.dto.response.WarehouseDTOResponse;
import com.api19_4.api19_4.dto.response.ObjectResponse;
import com.api19_4.api19_4.model.WarehouseMapper;
import com.api19_4.api19_4.services.servicePostgres.WarehouseService;
import com.api19_4.api19_4.util.SearchUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/api/v1/warehouses")
public class WarehouseController {
    private Logger logger = LoggerFactory.getLogger(WarehouseController.class);
    @Autowired
    private WarehouseMapper warehouseMapper;
    @Autowired
    private WarehouseService warehouseService;
    @Autowired
    private ModelMapper modelMapper;

    //==================tạo kho moi===============================
    @PostMapping("/create")
    public ResponseEntity<?> createWarehouse(@Valid @RequestBody CreateWarehouseDTORequest request, BindingResult result) {
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        return warehouseService.createWarehouse(request);
    }

    @PostMapping(value = "/upload-excel", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> createWarehouseFromExcel(@RequestPart("excelFile") MultipartFile excelFile) throws IOException {
        if (!isExcelFile(excelFile)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(false, "File format is not supported. Please upload an Excel file.", "")
            );
        }
        try (InputStream is = excelFile.getInputStream()) {
            Workbook workbook = new XSSFWorkbook(is);
            Sheet sheet = workbook.getSheetAt(0);
            return warehouseService.createWarehouseByExcel(sheet);
        } catch (Exception e) {
            logger.error("Error upload excel warehouse: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }
    private boolean isExcelFile(MultipartFile file) {
        return file.getOriginalFilename().endsWith(".xlsx");
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateWarehouse(@RequestBody WarehouseDTORequest warehouseDTO){
        return warehouseService.updateWarehouse(warehouseDTO);
    }

    @DeleteMapping("/{idWarehouse}")
    public ResponseEntity<?> deleteWarehouse(@PathVariable String idWarehouse) {
        return warehouseService.deleteWarehouse(idWarehouse);
    }

    @ApiOperation(value = "API tìm kiếm nâng cao kho hàng")
    @PostMapping("/roleAdmin/filter")
    @ResponseBody
    public PageResponse<WarehouseDTOResponse> advanceFilters(@Valid @RequestBody SearchWarehouseRequest searchWarehouseRequest,
                                                             @PositiveOrZero @RequestParam(required = false, defaultValue = "0") Integer page,
                                                             @Positive @RequestParam (required = false, defaultValue = "15") Integer size) throws Exception{
            Pageable pageable = SearchUtil.getPageableFromParamP(page, size);
            Page<WarehouseDTOResponse> pageData = warehouseService.advanceSearch(searchWarehouseRequest, pageable);
            return new PageResponse<>(pageData);

    }

    @GetMapping("/searchAll")
    public ResponseEntity<?> searchAllWarehouse(){
        return warehouseService.getAllWarehouses();
    }

}
