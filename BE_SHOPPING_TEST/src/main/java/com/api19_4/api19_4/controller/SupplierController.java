package com.api19_4.api19_4.controller;

import com.api19_4.api19_4.dto.request.CreateSupplierDTORequest;
import com.api19_4.api19_4.dto.request.SupplierDTORequest;
import com.api19_4.api19_4.dto.response.ObjectResponse;
import com.api19_4.api19_4.services.servicePostgres.SupplierService;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping("/api/v1/supplier")
public class SupplierController {
    private Logger logger = LoggerFactory.getLogger(SupplierController.class);
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private ModelMapper modelMapper;
    @PostMapping("/create")
    public ResponseEntity<?> createSupplier(@Valid @RequestBody CreateSupplierDTORequest supplierDTO , BindingResult result) {
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        return supplierService.create(supplierDTO);
    }

    @PostMapping(value = "/upload-excel", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> createSupplierFromExcel(@RequestPart("excelFile") MultipartFile excelFile) throws IOException {
        if (!isExcelFile(excelFile)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(false, "File format is not supported. Please upload an Excel file.", "")
            );
        }
        try (InputStream is = excelFile.getInputStream()) {
            Workbook workbook = new XSSFWorkbook(is);
            Sheet sheet = workbook.getSheetAt(0); // Giả sử dữ liệu nằm trên sheet đầu tiên
            return supplierService.createByExcel(sheet);
        }catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Failed to process Excel file.", "")
            );
        }
    }
    private boolean isExcelFile(MultipartFile file) {
        return file.getOriginalFilename().endsWith(".xlsx");
    }

    @GetMapping("/searchAll")
    public ResponseEntity<?> searchAllSupplier(){
        return supplierService.getAllSupplier();
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateSupplier(@RequestBody SupplierDTORequest supplierDTO){
        return supplierService.update(supplierDTO);
    }

    @DeleteMapping("/{idSupplier}")
    public ResponseEntity<?> deleteSupplier(@PathVariable String idSupplier) {
        return supplierService.delete(idSupplier);
    }


}
