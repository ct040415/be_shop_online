//package com.api19_4.api19_4.controller;
//
//import com.api19_4.api19_4.dto.response.PageResponse;
//import com.api19_4.api19_4.dto.request.ProductLikeDTORequest;
//import com.api19_4.api19_4.dto.request.SearchProductLikeRequest;
//import com.api19_4.api19_4.dto.response.ProductLikeDTOResponse;
//import com.api19_4.api19_4.dto.response.ObjectResponse;
//import com.api19_4.api19_4.services.serviceSqlserver.ProductLikeService;
//import com.api19_4.api19_4.util.SearchUtil;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//import javax.validation.constraints.Positive;
//import javax.validation.constraints.PositiveOrZero;
//import java.util.*;
//
//@RestController
//@RequestMapping(path = "/api/v6/ProdLike")
//@SpringBootApplication
//@ComponentScan(basePackages = "com.api19_4.api19_4")
//public class ProductLikeController {
//
//    @Autowired
//    private ProductLikeService prodLikeService;
//    @GetMapping("")
//    public ResponseEntity<?> getAllProdLike() {
//        try {
//            List<ProductLikeDTOResponse> l = prodLikeService.getAllProductLike();
//            return ResponseEntity.status(HttpStatus.OK).body(
//                    new ObjectResponse(true, "Get all ProductLike successfully", l)
//            );
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
//        }
//    }
//
//    @PostMapping("")
//    public ResponseEntity<?> createProductLike(@RequestBody ProductLikeDTORequest productLikeDTORequest) {
//        try {
//            ProductLikeDTOResponse p = prodLikeService.createProductLike(productLikeDTORequest);
//            return ResponseEntity.status(HttpStatus.OK).body(
//                    new ObjectResponse(true, "Create ProductLike successfully", p)
//            );
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred while creating the product like.");
//        }
//    }
//
//    @DeleteMapping("")
//    public ResponseEntity<?> deleteProductLike(@PathVariable String idProduct, @PathVariable String idUser) {
//        try {
//            prodLikeService.deleteProductLike(idProduct, idUser);
//            return ResponseEntity.status(HttpStatus.OK).body(
//                    new ObjectResponse(true, "Delete ProductLike with IDProduct " + idProduct + " and IDUser " + idUser + " successfully", "")
//            );
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
//        }
//    }
//
//    @DeleteMapping("allByIdUser/{idUser}")
//        ResponseEntity<ObjectResponse> deleteAllShoppingCart(@PathVariable String idUser){
//        try {
//            prodLikeService.deleteAllByIdUser(idUser);
//            return ResponseEntity.status(HttpStatus.OK).body(
//                    new ObjectResponse(true, "Delete ProductLike with IDUser " + idUser + " successfully", "")
//            );
//        }
//        catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
//        }
//    }
//
//    @GetMapping("/check")
//    public ResponseEntity<Boolean> checkProductLike(
//            @RequestParam String idUser,
//            @RequestParam String idProd
//    ) {
//        try {
//            return ResponseEntity.ok(prodLikeService.checkProductLike(idProd, idUser));
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(false);
//        }
//    }
//
//    @ApiOperation(value = "API tìm kiếm nâng cao")
//    @PostMapping("/roleAdmin/filter")
//    @ResponseBody
//    public PageResponse<ProductLikeDTOResponse> advanceFilters(@Valid @RequestBody SearchProductLikeRequest searchProductLikeRequest,
//                                                               @PositiveOrZero @RequestParam(required = false, defaultValue = "0") Integer page,
//                                                               @Positive @RequestParam (required = false, defaultValue = "15") Integer size) throws Exception{
//        Pageable pageable = SearchUtil.getPageableFromParamP(page, size);
//        Page<ProductLikeDTOResponse> pageData = prodLikeService.advanceSearch(searchProductLikeRequest, pageable);
//        return new PageResponse<>(pageData);
//
//    }
//}
