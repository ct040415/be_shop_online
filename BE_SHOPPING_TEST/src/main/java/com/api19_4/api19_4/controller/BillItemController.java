package com.api19_4.api19_4.controller;

import com.api19_4.api19_4.services.servicePostgres.BillItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "/api/v6/ProdBill")
@SpringBootApplication
@ComponentScan(basePackages = "com.api19_4.api19_4") // Thay thế bằng package chứa các bean của ứng dụng của bạn
public class BillItemController {
    @Autowired
    private BillItemService billItemService;

}
