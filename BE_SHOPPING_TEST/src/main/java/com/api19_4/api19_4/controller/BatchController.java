package com.api19_4.api19_4.controller;


import com.api19_4.api19_4.dto.request.BatchDTORequest;
import com.api19_4.api19_4.dto.request.CreateBatchDTORequest;
import com.api19_4.api19_4.services.servicePostgres.BatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path ="/api/v1/batches")
@Validated
public class BatchController {
    @Autowired
    private BatchService batchService;
    @PostMapping("/create")
    public ResponseEntity<?> createBatch(@Valid @RequestBody CreateBatchDTORequest batchDTO) {
        try {
            return batchService.createBatch(batchDTO);
        } catch (Exception e) {
            return new ResponseEntity<>("Error occurred while creating the Batch", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PutMapping("/update")
    public ResponseEntity<?> updateBatch(@RequestBody BatchDTORequest batchDTO){
        try {
            return batchService.updateBatch(batchDTO);
        } catch (Exception e) {
            return new ResponseEntity<>("Error occurred while updating the Batch", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/getAll")
    public ResponseEntity<?> getAllBatch(){
        try {
            return batchService.getAllBatch();
        } catch (Exception e) {
            return new ResponseEntity<>("Error occurred while getting the Batch", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{idBatch}")
    public ResponseEntity<?> deleteBatch(@PathVariable String idBatch){
        try {
            return batchService.deleteBatch(idBatch);
        } catch (Exception e) {
            return new ResponseEntity<>("Error occurred while deleting the Batch", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}