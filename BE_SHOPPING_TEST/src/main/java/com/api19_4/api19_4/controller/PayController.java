package com.api19_4.api19_4.controller;

import com.api19_4.api19_4.config.VNPayConfig;
import com.api19_4.api19_4.services.servicePostgres.impl.VNPayService;
import com.api19_4.api19_4.util.VNPayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@Controller
@RestController
@RequestMapping(path = "/api/v9/pay")
public class PayController {
    private Logger logger = LoggerFactory.getLogger(PayController.class);
    @Autowired
    private VNPayService vnPayService;

    @GetMapping("")
    public String home(){
        return "index";
    }

    @PostMapping("/submitOrder")
    public String submidOrder(@RequestParam("amount") int orderTotal,
                            @RequestParam("orderInfo") String orderInfo,
                            HttpServletRequest request){
        String baseUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        String vnpayUrl = vnPayService.createOrder(orderTotal, orderInfo, baseUrl);
        return "redirect:" + vnpayUrl;
    }

    @GetMapping("/api/v9/pay/vnpay-payment")
    public ResponseEntity<?> handleVNPayResponse(@RequestParam Map<String, String> allParams) {

        logger.info("Received VNPay response: {}", allParams);

        if (VNPayUtils.validateSignature(allParams, VNPayConfig.vnp_HashSecret)) {
            // Handle the payment response
            String transactionStatus = allParams.get("vnp_TransactionStatus");
            if ("00".equals(transactionStatus)) {
                // Payment was successful
                return ResponseEntity.ok().body("Payment successful");
            } else {
                // Payment failed
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Payment failed");
            }
        } else {
            // Invalid signature
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid signature");
        }
    }

    @GetMapping("/vnpay-payment")
    public String GetMapping(HttpServletRequest request, Model model) {
        int paymentStatus = vnPayService.orderReturn(request);

        String orderInfo = request.getParameter("vnp_OrderInfo");
        String paymentTime = request.getParameter("vnp_PayDate");
        String transactionId = request.getParameter("vnp_TransactionNo");
        String totalPrice = request.getParameter("vnp_Amount");

        model.addAttribute("orderId", orderInfo);
        model.addAttribute("totalPrice", totalPrice);
        model.addAttribute("paymentTime", paymentTime);
        model.addAttribute("transactionId", transactionId);

        return paymentStatus == 1 ? "ordersuccess" : "orderfail";
    }
}