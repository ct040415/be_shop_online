package com.api19_4.api19_4.controller;

import com.api19_4.api19_4.dto.request.*;
import com.api19_4.api19_4.dto.response.JwtResponse;
import com.api19_4.api19_4.dto.response.ObjectResponse;
import com.api19_4.api19_4.entity.entityPostgres.RefreshToken;
import com.api19_4.api19_4.services.servicePostgres.impl.JwtService;
import com.api19_4.api19_4.services.servicePostgres.impl.RefreshTokenService;
import com.api19_4.api19_4.services.servicePostgres.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.util.*;


@Controller
@RestController
@RequestMapping(path = "/api/v2/users/")
@Validated
public class UserController {
    private Long loginTimeMillis;
    @Autowired
    private UserService userService;
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private RefreshTokenService refreshTokenService;

    //======================================== register ================================
    @PostMapping(path = "/signupUser")
    public ResponseEntity<?> signupUser(@Valid @RequestBody UserDtoSignUpRequest userDto, BindingResult result, HttpServletRequest httpServletRequest) {
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        System.out.println("======> httpServletRequest.getRemoteAddr(): "+httpServletRequest.getRemoteAddr());
        return userService.signUpUser(userDto);
    }

    @PostMapping(path = "/signupAdmin")
    public ResponseEntity<?> signupAdmin(@Valid @RequestBody UserDtoSignUpRequest userDto, BindingResult result) {
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        return userService.signupByAdmin(userDto);
    }

    //======================================== login ================================
    @PostMapping(path = "user/login")

    public ResponseEntity<?> loginUser(@Valid @RequestBody UserDtoLoginRequest loginDto, BindingResult result){
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        return userService.loginUser(loginDto);
    }

    //    ==================================Update User============================================
    @PutMapping("/updateUser")
    public ResponseEntity<?> updateUser(@Valid @RequestBody UserDTOInfoRequest userDTOInfoRequest, BindingResult result
    ){
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        return userService.updateUser(userDTOInfoRequest);
    }

    //    ==================================Change Pass============================================
    @PutMapping("/changePassword")
    public ResponseEntity<?> changePasswordByEmail(@Valid @RequestBody UserDTOChangePassRequest userDTOChangePassRequest,BindingResult result) {
        if (result.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ObjectResponse response = new ObjectResponse(false, "Validation failed", errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        return userService.changePass(userDTOChangePassRequest);
    }

    //======================================== search user================================
    @GetMapping("/roleAdmin/getAllUser")
    public ResponseEntity<?> getAllUsers(){
        return userService.getAllUser();
    }

    //======================================== get user================================
    @GetMapping("/id")
    ResponseEntity<?> searchUserId(@RequestParam String idUser){
        return userService.getUserById(idUser);
    }

    @GetMapping("/getUsernameByEmail")
    public ResponseEntity<?> getUsernameByEmail(@RequestParam @Email(message = "Invalid email format") String email) {
        return userService.getUserByEMail(email);
    }

    //======================================== delete ================================
    @DeleteMapping("roleAdmin/{id}")
    ResponseEntity<?> deleteUser(@PathVariable String id){
        return userService.deleteUser(id);
    }

    //======================================== Refresh token ================================
    @PostMapping(path = "refreshToken")
    public JwtResponse refreshToken(@RequestBody RefreshTokenRequest refreshTokenRequest) {
        return refreshTokenService.findByToken(refreshTokenRequest.getToken())
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(userInfo -> {
                    String accessToken = jwtService.generateToken(userInfo.getUserName());
                    return JwtResponse.builder()
                            .accessToken(accessToken)
                            .token(refreshTokenRequest.getToken())
                            .expirationTime(new Date(System.currentTimeMillis()+1000*60*2).toInstant())
                            .build();
                }).orElseThrow(() -> new RuntimeException(
                        "Refresh token is not in database!"));
    }


}
