package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;


@Entity
@Setter
@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idBillItem")
@Table(name="BillItem")
public class BillItem {

    @Id
    private String idBillItem;

    private int quantity;

    private int size;

    private int discount;

    private double totalPriceProd;

    @OneToOne
    @JoinColumn(name = "idProd")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "idBill")
    @JsonBackReference
    private Bill bill;

    public BillItem(IDGenerator idGenerator){
        this.idBillItem = idGenerator.generateNextID();
    }
}
