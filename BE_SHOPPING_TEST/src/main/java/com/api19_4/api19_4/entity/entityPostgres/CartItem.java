package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Setter
@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idCartItem")
@Table(name="CartItem")
public class CartItem {
    @Id
    private String idCartItem;

    private int quantity;

    private int size;

    private double totalPriceProd;

    @OneToOne
    @JoinColumn(name = "idProd")
    private Product product;

    public CartItem(IDGenerator idGenerator){
        this.idCartItem = idGenerator.generateNextID();
    }
}
