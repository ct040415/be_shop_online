package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.enums.SenderTypeEnum;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Component
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idChatBot")
@Table(name="ChatBot")
public class ChatBot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idChatBot;

    @Column(columnDefinition = "varchar(1000)")
    private String content;

    private String image;

    @Enumerated(EnumType.STRING)
    private SenderTypeEnum sender;

    private String timestamp;

    private String time;

    @Column(columnDefinition = "varchar(1000)")
    private String url;

    @Column(columnDefinition = "varchar(1000)")
    private String productName;

    @Column(columnDefinition = "varchar(1000)")
    private String brand;

    private int size;

    private double priceFrom;

    private double priceTo;

    @Column(columnDefinition = "varchar(1000)")
    private String categoryName;

    private boolean coupons;

    private boolean display;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserInfo user;
}
