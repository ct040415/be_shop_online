package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.Constants;
import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idUser")
@Table(name = "users")
public class UserInfo {
    @Id
    @Column(name = "idUser")
    private String idUser;

    @Size(max = Constants.NAME_MAX_LENGTH, min = Constants.NAME_MIN_LENGTH)
    @Column(unique = true, columnDefinition = "varchar(1000)")
    private String userName;

    @Size(max = Constants.PASSWORD_MAX_LENGTH, min = Constants.PASSWORD_MIN_LENGTH)
    private String password;

    @Column(columnDefinition = "varchar(1000)")
    @Size(max = Constants.EMAIL_MAX_LENGTH)
    @Pattern(regexp = Constants.PATTERN_EMAIL)
    private String email;

    @Pattern(regexp = Constants.PATTERN_PHONENUMBER)
    private String phoneNumber;

    @Column(columnDefinition = "varchar(1000)")
    private String address;

    private String role;

    private Boolean is_delete = false;

    @Column(columnDefinition = "varchar(1000)")
    private String gender;

    @Column(columnDefinition = "varchar(1000)")
    private String avatar;

    @Column(columnDefinition = "varchar(1000)")
    private String background;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Bill> bills;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ChatBot> messages;

    public UserInfo(IDGenerator idGenerator){
        this.idUser = idGenerator.generateNextID();
    }
}
