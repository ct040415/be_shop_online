package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Component
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idBatchItem")
@Table(name = "BatchItem")
public class BatchItem {
    @Id
    private String idBatchItem;

    private double purchaseUnitPrice;

    private double retailUnitPrice;

    private int totalQuantity;

    private double totalPurchasePrice;

    private Integer discount;

    private String color;

    private int size;

    @Column(columnDefinition = "varchar(1000)")
    private String note;

    private String status;

    @ManyToOne
    @JoinColumn(name = "batch_id")
    private Batch batch;

    @OneToOne
    @JoinColumn(name = "product_id")
    private Product product;

    public BatchItem(IDGenerator idGenerator){
        this.idBatchItem = idGenerator.generateNextID();
    }

}
