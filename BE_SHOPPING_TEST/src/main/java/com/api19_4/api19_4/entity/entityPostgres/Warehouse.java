package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Component
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idWarehouse")
@Table(name = "Warehouse")
public class Warehouse {
    @Id
    private String idWarehouse;

    @Column(columnDefinition = "varchar(1000)")
    private String name;

    @Column(columnDefinition = "varchar(1000)")
    private String address;

    private Boolean is_delete = false;

    @Column(columnDefinition = "varchar(1000)")
    private String information;

    @OneToMany(mappedBy = "warehouse", cascade = CascadeType.ALL)
    private List<Batch> batches;

    @OneToMany(mappedBy = "warehouse")
    private List<InventoryCheck> inventoryChecks;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "warehouse_product",
            joinColumns = @JoinColumn(name = "warehouse_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    private List<Product> products; // Many-to-many relationship with products

    public Warehouse(IDGenerator idGenerator){
        this.idWarehouse = idGenerator.generateNextID();
    }

}
