package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.Constants;
import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.firebase.database.annotations.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Bill")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idBill")
public class Bill {

    @Id
    private String idBill;

    @NotNull
    @Pattern(regexp = Constants.PATTERN_PHONENUMBER)
    private String numberPhoneCustomer;

    @Column(columnDefinition = "varchar(1000)")
    private String addressCustomer;

    private LocalDateTime dateTimeOrder;

    private String status;

    private double totalPayment;

    private double payableAmount;

    private double shippingFee;

    @Column (columnDefinition = "varchar(1000)")
    private String note;

    @OneToMany(mappedBy = "bill", cascade = CascadeType.ALL)
    private List<BillItem> billItems;

    @ManyToOne
    @JoinColumn(name = "idUser")
    private UserInfo user;

    public Bill(IDGenerator idGenerator){
        this.idBill = idGenerator.generateNextID();
    }
}
