package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Entity
@Component
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idProd")
@Table(name="Product")
public abstract class Product {
    @Id
    @Column(name = "idProd")
    private String idProd;

    @Column(columnDefinition = "varchar(1000)")
    private String productName;

    private double retailPrice;

    @Column(columnDefinition = "varchar(1000)")
    private String description;

    @Column(columnDefinition = "varchar(1000)")
    private String brand;

    private int quantityInStock;

    private int quantitySold;

    private int quantityInAvailable;

    private int quantityOrdered;

    private int coupons;

    private String productType;

    @OneToMany(mappedBy = "product")
    private List<Batch> batches;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<BatchItem> batchItems;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "Product_Images", joinColumns = @JoinColumn(name = "idProd"))
    @Column(name = "image")
    private List<String> images;

    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private Supplier supplier;

    @ManyToMany(mappedBy = "products", fetch = FetchType.LAZY)
    private List<Warehouse> warehouses;

    @OneToOne
    @JoinColumn(name = "idProdCheck")
    private InventoryCheckItem inventoryCheckItem;

    public Product(IDGenerator idGenerator) {
        this.idProd = idGenerator.generateNextID();
    }

}
