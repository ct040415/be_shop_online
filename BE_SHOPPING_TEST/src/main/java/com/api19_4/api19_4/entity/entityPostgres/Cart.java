package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@Entity
@Component
@AllArgsConstructor
@NoArgsConstructor
@Table(name="Cart")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idCart")
public class Cart {

    @Id
    private String idCart;

    private int totalQuantity;

    private double totalPrice;

    @OneToOne
    @JoinColumn(name = "idUser")
    private UserInfo userInfo;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "cart",
            joinColumns = @JoinColumn(name = "idCart"),
            inverseJoinColumns = @JoinColumn(name = "idProd")
    )
    private List<Product> products;

    public Cart(IDGenerator idGenerator){
        this.idCart = idGenerator.generateNextID();
    }
}
