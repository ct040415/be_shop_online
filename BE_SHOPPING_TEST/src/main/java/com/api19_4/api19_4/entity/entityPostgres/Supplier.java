package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.Constants;
import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Component
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idSupplier")
@Table(name = "Supplier")
public class Supplier {
    @Id
    private String idSupplier;

    @Column(columnDefinition = "varchar(1000)")
    private String name;

    @Column(columnDefinition = "varchar(1000)")
    private String information;

    @Column(columnDefinition = "varchar(1000)")
    private String address;

    @OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL)
    private List<Batch> batches;

    public Supplier(IDGenerator idGenerator){
        this.idSupplier = idGenerator.generateNextID();
    }

}
