package com.api19_4.api19_4.entity.entityPostgres;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Entity
@Component
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name="ShoeProduct")
public class ShoeProduct extends Product{

    @ElementCollection
    @CollectionTable(name = "list_color", joinColumns = @JoinColumn(name = "color_id"))
    @Column(name = "color")
    private List<String> color;

    @ElementCollection
    @CollectionTable(name = "list_size", joinColumns = @JoinColumn(name = "size_id"))
    @Column(name = "size")
    private List<Integer> size;
}
