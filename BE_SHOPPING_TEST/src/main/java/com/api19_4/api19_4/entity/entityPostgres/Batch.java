package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idBatch")
@Table(name = "Batch")
public class Batch {
    @Id
    private String idBatch;

    @Column(columnDefinition = "varchar(1000)")
    private String name;

    private LocalDateTime dateImport;

    private LocalDateTime manufacturingDate;

    private LocalDateTime expirationDate;

    private double payableAmount;

    private double paidAmount;

    @Column(columnDefinition = "varchar(1000)")
    private String orderInitiator;

    @OneToMany(mappedBy = "batch")
    private List<BatchItem> batchItems;

    @ManyToOne
    @JoinColumn(name = "warehouse_id")
    private Warehouse warehouse;

    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private Supplier supplier;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    public Batch(IDGenerator idGenerator){
        this.idBatch = idGenerator.generateNextID();
    }

    public String getDateImportFormatted() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy");
        return dateImport.format(formatter);
    }
}
