package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Component
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idInventoryCheck")
@Table(name = "InventoryCheck")
public class InventoryCheck {
    @Id
    private String idInventoryCheck;

    private LocalDateTime timeCheck;

    private LocalDateTime balanceDate;

    private int actualQuantity;

    private double totalActualValue;

    private int totalQuantityDeviation;

    private int increasedDeviation;

    private int decreasedDeviation;

    @Column(columnDefinition = "varchar(1000)")
    private String note;

    private String status;

    @OneToMany(mappedBy = "inventoryCheck", cascade = CascadeType.ALL)
    private List<InventoryCheckItem> inventoryCheckItems;

    @ManyToOne
    @JoinColumn(name = "warehouse_id")
    private Warehouse warehouse;

    public InventoryCheck(IDGenerator idGenerator){
        this.idInventoryCheck = idGenerator.generateNextID();
    }
}
