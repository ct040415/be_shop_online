//package com.api19_4.api19_4.entity.entitySqlserver;
//
//import com.api19_4.api19_4.util.IDGenerator;
//import com.fasterxml.jackson.annotation.JsonIdentityInfo;
//import com.fasterxml.jackson.annotation.ObjectIdGenerators;
//import lombok.AllArgsConstructor;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//import org.hibernate.annotations.GenericGenerator;
//import org.springframework.stereotype.Component;
//
//import javax.persistence.*;
//
//
//@Entity
//@Setter
//@Getter
//@Component
//@AllArgsConstructor
//@NoArgsConstructor
//@Table(name="ProductLike")
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idProdLike")
//public class ProductLike {
//
//    @Id
//    @SequenceGenerator(
//            name = "productLike_sequence",
//            sequenceName = "productLike_sequence",
//            allocationSize = 1 // increment by 1
//    )
//    @GeneratedValue(
//            strategy = GenerationType.SEQUENCE,
//            generator = "productLike_sequence"
//    )
//    private String idProdLike;
//
//    private String idProd;
//
//    private String idUser;
//
//}
