package com.api19_4.api19_4.entity.entityPostgres;

import com.api19_4.api19_4.util.IDGenerator;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Component
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idInventoryCheckItem")
@Table(name="InventoryCheckItem")
public class InventoryCheckItem {
    @Id
    private String idInventoryCheckItem;

    private int quantityActual;

    private int quantityDeviation;

    private int quantityInStock;

    private double valueDeviation;

    @OneToOne
    @JoinColumn(name = "idProd")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "idInventoryCheck")
    @JsonBackReference
    private InventoryCheck inventoryCheck;

    @ManyToOne
    @JoinColumn(name = "warehouse_id")
    private Warehouse warehouse;

    public InventoryCheckItem(IDGenerator idGenerator){
        this.idInventoryCheckItem = idGenerator.generateNextID();
    }
}
