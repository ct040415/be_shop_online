package com.api19_4.api19_4.repositories.repoPostgres;

import com.api19_4.api19_4.entity.entityPostgres.Bill;
import com.api19_4.api19_4.entity.entityPostgres.UserInfo;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Repository
@Transactional
public interface BillRepository extends JpaRepository<Bill, String>, JpaSpecificationExecutor<Bill> {

    List<Bill> findAll(Specification<Bill> spec);

    List<Bill> findByDateTimeOrderBetween(LocalDateTime hourStart, LocalDateTime hourEnd);


    List<Bill> findByUser(UserInfo user);

    Bill findTopByOrderByIdBillDesc();

    List<Bill> findBillsByUser_IdUser(String userId);
}
