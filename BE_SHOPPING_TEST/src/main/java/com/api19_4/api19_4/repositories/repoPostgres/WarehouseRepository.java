package com.api19_4.api19_4.repositories.repoPostgres;

import com.api19_4.api19_4.entity.entityPostgres.Warehouse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WarehouseRepository extends JpaRepository<Warehouse, String>, JpaSpecificationExecutor<Warehouse> {

    Optional<Warehouse> findById(String warehouseId);

    @Query("SELECT COUNT(w) FROM Warehouse w")
    int countAllWareHouses();

    Warehouse findTopByOrderByIdWarehouseDesc();

    Page<Warehouse> findAll(Specification<Warehouse> spec, Pageable pageable);

    Warehouse findByName(String name);
}
