package com.api19_4.api19_4.repositories.repoPostgres;

import com.api19_4.api19_4.entity.entityPostgres.RefreshToken;
import com.api19_4.api19_4.entity.entityPostgres.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
public interface RefreshTokenRepository extends JpaRepository<RefreshToken,Integer> {
    Optional<RefreshToken> findByToken(String token);

    Optional<RefreshToken> findByUser(UserInfo user);
}