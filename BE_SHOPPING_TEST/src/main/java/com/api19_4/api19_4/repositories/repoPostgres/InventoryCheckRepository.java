package com.api19_4.api19_4.repositories.repoPostgres;

import com.api19_4.api19_4.entity.entityPostgres.Bill;
import com.api19_4.api19_4.entity.entityPostgres.InventoryCheck;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface InventoryCheckRepository extends JpaRepository<InventoryCheck, String > {
    @Query("SELECT COUNT(ic) FROM InventoryCheck ic")
    int countAllInventoryChecks();

    List<InventoryCheck> findAll(Specification<Bill> spec);
}
