package com.api19_4.api19_4.repositories.repoPostgres;

import com.api19_4.api19_4.entity.entityPostgres.BatchItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface BatchItemRepository extends JpaRepository<BatchItem, String>, JpaSpecificationExecutor<BatchItem> {
    @Query("SELECT COUNT(b) FROM BatchItem b")
    int countAllBatchItems();

    BatchItem findTopByOrderByIdBatchItemDesc();
}
