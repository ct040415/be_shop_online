package com.api19_4.api19_4.repositories.repoPostgres;


import com.api19_4.api19_4.entity.entityPostgres.Batch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface BatchRepository extends JpaRepository<Batch, String>, JpaSpecificationExecutor<Batch> {
    List<Batch> findByName(String name);

    Optional<Batch> findById(String idBatch);

    @Query("SELECT COUNT(b) FROM Batch b")
    int countAllBatchs();

    Batch findTopByOrderByIdBatchDesc();

}
