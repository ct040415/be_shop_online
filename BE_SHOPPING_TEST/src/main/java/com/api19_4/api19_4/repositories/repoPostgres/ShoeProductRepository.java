package com.api19_4.api19_4.repositories.repoPostgres;

import com.api19_4.api19_4.dto.response.ShoeProductDtoResponse;
import com.api19_4.api19_4.entity.entityPostgres.Product;
import com.api19_4.api19_4.entity.entityPostgres.ShoeProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ShoeProductRepository extends JpaRepository<ShoeProduct, String>, JpaSpecificationExecutor<ShoeProduct> {
    Page<ShoeProduct> findAll(Specification<ShoeProduct> spec, Pageable pageable);

    List<ShoeProduct> findByCouponsNot(int i);
    @Query("SELECT COUNT(s) FROM ShoeProduct s")
    int countAllShoeProducts();
    ShoeProduct findTopByOrderByIdProdDesc();

    ShoeProduct findByProductName(String productNameStr);
}
