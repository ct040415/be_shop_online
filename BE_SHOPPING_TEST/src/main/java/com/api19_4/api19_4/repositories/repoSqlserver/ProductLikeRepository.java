//package com.api19_4.api19_4.repositories.repoSqlserver;
//
//import com.api19_4.api19_4.entity.entitySqlserver.ProductLike;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
//import org.springframework.stereotype.Repository;
//
//import javax.transaction.Transactional;
//import java.util.List;
//import java.util.Optional;
//
//@Repository
//@Transactional
//public interface ProductLikeRepository extends JpaRepository<ProductLike, String>, JpaSpecificationExecutor<ProductLike> {
//    @Override
//    List<ProductLike> findAll();
//
//
//    List<ProductLike> findByIdUser(String idUser);
//
//    long count();
//
////    Warehouse findTopByOrderByIdProdDesc();
//
//    Optional<ProductLike> findByIdUserAndIdProd(String idUser, String idProd);
//
////    ProductLike findTopByOrderByProduct_IdProdDesc();
//}
