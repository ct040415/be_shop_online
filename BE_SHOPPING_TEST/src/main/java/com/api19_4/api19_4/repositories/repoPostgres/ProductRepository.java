package com.api19_4.api19_4.repositories.repoPostgres;

import com.api19_4.api19_4.dto.response.ProductDTOResponse;
import com.api19_4.api19_4.entity.entityPostgres.Product;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface ProductRepository extends JpaRepository<Product, String>, JpaSpecificationExecutor<Product> {

    List<Product> findByCouponsNot(float coupons);
    Optional<Product> findByIdProd(String idProd);
    boolean existsByIdProd(String idProd);

    void deleteByIdProd(String idProd);
    @Query("SELECT COUNT(p) FROM Product p")
    int countAllProducts();
    @EntityGraph(attributePaths = "images", type = EntityGraph.EntityGraphType.FETCH)
    @Query("SELECT DISTINCT p FROM Product p")
    List<Product> findAllWithAllImages();

    Product findTopByOrderByIdProdDesc();

}
