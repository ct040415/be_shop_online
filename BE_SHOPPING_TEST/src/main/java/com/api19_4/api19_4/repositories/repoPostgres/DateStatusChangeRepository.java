package com.api19_4.api19_4.repositories.repoPostgres;

import com.api19_4.api19_4.entity.entityPostgres.DateStatusChange;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface DateStatusChangeRepository extends JpaRepository<DateStatusChange, String>, JpaSpecificationExecutor<DateStatusChange> {
    List<DateStatusChange> findAll(Specification<DateStatusChange> spec);
}
