//package com.api19_4.api19_4.services.serviceSqlserver.impl;
//
//import com.api19_4.api19_4.dto.request.ProductLikeDTORequest;
//import com.api19_4.api19_4.dto.request.SearchProductLikeRequest;
//import com.api19_4.api19_4.dto.response.ProductLikeDTOResponse;
//import com.api19_4.api19_4.entity.entitySqlserver.ProductLike;
//import com.api19_4.api19_4.repositories.repoSqlserver.ProductLikeRepository;
//import com.api19_4.api19_4.services.serviceSqlserver.ProductLikeService;
//import com.api19_4.api19_4.model.ProductLikeMapper;
//import com.api19_4.api19_4.util.SearchUtil;
//import com.api19_4.api19_4.util.Validator;
//import org.modelmapper.ModelMapper;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageImpl;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.domain.Specification;
//import org.springframework.stereotype.Service;
//
//import javax.validation.Valid;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//import java.util.Optional;
//
//@Service
//public class ProductLikeServiceImpl implements ProductLikeService {
//
//    @Autowired
//    private ProductLikeRepository productLikeRepository;
//    @Autowired
//    private ModelMapper modelMapper;
//    @Autowired
//    private ProductLikeMapper productLikeMapper;
//
//    @Override
//    public ProductLikeDTOResponse createProductLike(ProductLikeDTORequest productLikeDTORequest) {
//        ProductLike productLike = new ProductLike();
//        modelMapper.map(productLikeDTORequest, productLike);
//        ProductLike p = productLikeRepository.save(productLike);
//        return modelMapper.map(p, ProductLikeDTOResponse.class);
//    }
//
//    @Override
//    public ProductLikeDTOResponse updateProductLike(ProductLikeDTORequest productLikeDTORequest) {
//        ProductLike productLike = productLikeRepository.findById(productLikeDTORequest.getIdProd())
//                .orElseThrow(() -> new RuntimeException("ProductLike with id " + productLikeDTORequest.getIdProd() + " not found"));
//
//        modelMapper.map(productLikeDTORequest, productLike);
//        ProductLike p = productLikeRepository.save(productLike);
//        return modelMapper.map(p, ProductLikeDTOResponse.class);
//    }
//
//    @Override
//    public void deleteProductLike(String idProd, String idUser) {
//        ProductLike productLike = productLikeRepository.findByIdUserAndIdProd(idUser,idProd)
//                .orElseThrow(() -> new RuntimeException("ProductLike with id " + idProd + " or " + idUser + " not found"));
//        productLikeRepository.delete(productLike);
//    }
//
//    @Override
//    public void deleteAllByIdUser(String idUser) {
//        List<ProductLike> productLikes = productLikeRepository.findByIdUser(idUser);
//        for(ProductLike p: productLikes){
//            productLikeRepository.delete(p);
//        }
//    }
//
//    @Override
//    public boolean checkProductLike(String idProd, String idUser) {
//        Optional<ProductLike> productLike = productLikeRepository.findByIdUserAndIdProd(idUser, idProd);
//        return productLike.isPresent();
//    }
//
//    @Override
//    public List<ProductLikeDTOResponse> getAllProductLike() {
//        List<ProductLike> productLikes = productLikeRepository.findAll();
//        return productLikes.isEmpty() ? Collections.emptyList() : productLikeMapper.toDto(productLikes);
//    }
//
//    @Override
//    public Page<ProductLikeDTOResponse> findAll(Pageable pageable) throws Exception {
//        Page<ProductLike> page = productLikeRepository.findAll(pageable);
//        List<ProductLikeDTOResponse> ls = new ArrayList<>();
//        for (ProductLike u : page) {
//            ProductLikeDTOResponse productLikeDTOResponse = new ProductLikeDTOResponse();
//            BeanUtils.copyProperties(u, productLikeDTOResponse);
//            ls.add(productLikeDTOResponse);
//        }
//        long totalElements = page.getTotalElements();
//        return new PageImpl<>(ls, page.getPageable(), totalElements);
//    }
//
//    @Override
//    public Page<ProductLikeDTOResponse> advanceSearch(SearchProductLikeRequest searchProductLikeRequest, Pageable pageable) throws Exception {
//        if (searchProductLikeRequest != null) {
//            List<Specification<ProductLike>> specList = getAdvanceSearchSpecList(searchProductLikeRequest);
//            if (!specList.isEmpty()) {
//                Specification<ProductLike> spec = specList.get(0);
//                for (int i = 1; i < specList.size(); i++) {
//                    spec = spec.and(specList.get(i));
//                }
//                Page<ProductLike> page = productLikeRepository.findAll(spec, pageable);
//                List<ProductLikeDTOResponse> ls = new ArrayList<>();
//                for (ProductLike productLike : page) {
//                    ProductLikeDTOResponse productLikeDTOResponse = new ProductLikeDTOResponse();
//                    BeanUtils.copyProperties(productLike, productLikeDTOResponse);
//                    ls.add(productLikeDTOResponse);
//                }
//                long totalElements = page.getTotalElements();
//                return new PageImpl<>(ls, page.getPageable(), totalElements);
//            }
//        }
//        return findAll(pageable);
//    }
//
//    private List<Specification<ProductLike>> getAdvanceSearchSpecList(@Valid SearchProductLikeRequest s) {
//        List<Specification<ProductLike>> specList = new ArrayList<>();
//        // advance
//        if (Validator.isHaveDataString(s.getIdUser())) {
//            specList.add(SearchUtil.eq("idUser", s.getIdUser()));
//        }
//        if (Validator.isHaveDataString(s.getIdProd())) {
//            specList.add(SearchUtil.eq("idProd", s.getIdProd() ));
//        }
//        return specList;
//    }
//}
