package com.api19_4.api19_4.services.servicePostgres;

import com.api19_4.api19_4.dto.request.BatchItemDTORequest;
import com.api19_4.api19_4.dto.request.CreateBatchItemDTORequest;
import com.api19_4.api19_4.entity.entityPostgres.Batch;
import com.api19_4.api19_4.entity.entityPostgres.BatchItem;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BatchItemService {
    List<BatchItem> createListBatchItem(List<CreateBatchItemDTORequest> b, Batch batch, String idWH, String idSP);
    List<BatchItem> updateListBatchItem(List<BatchItemDTORequest> b);

}
