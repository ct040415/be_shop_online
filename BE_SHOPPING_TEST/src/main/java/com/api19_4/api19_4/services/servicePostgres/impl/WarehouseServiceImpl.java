package com.api19_4.api19_4.services.servicePostgres.impl;

import com.api19_4.api19_4.dto.request.CreateWarehouseDTORequest;
import com.api19_4.api19_4.dto.request.SearchWarehouseRequest;
import com.api19_4.api19_4.dto.request.WarehouseDTORequest;
import com.api19_4.api19_4.dto.response.ObjectResponse;
import com.api19_4.api19_4.dto.response.WarehouseDTOResponse;
import com.api19_4.api19_4.entity.entityPostgres.UserInfo;
import com.api19_4.api19_4.entity.entityPostgres.Warehouse;
import com.api19_4.api19_4.repositories.repoPostgres.WarehouseRepository;
import com.api19_4.api19_4.util.IDGenerator;
import com.api19_4.api19_4.model.WarehouseMapper;
import com.api19_4.api19_4.services.servicePostgres.WarehouseService;
import com.api19_4.api19_4.util.SearchUtil;
import com.api19_4.api19_4.util.Validator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class WarehouseServiceImpl implements WarehouseService {
    private Logger logger = LoggerFactory.getLogger(WarehouseServiceImpl.class);
    @Autowired
    private WarehouseRepository warehouseRepository;
    @Autowired
    private WarehouseMapper warehouseMapper;
    @Autowired
    private ModelMapper modelMapper;


    @Override
    public ResponseEntity<?> createWarehouse(CreateWarehouseDTORequest warehouseDTO) {
        try {
            logger.info("=============================Attempting to create warehouse");
            IDGenerator id = new IDGenerator("WH", getID());

            Warehouse warehouse = new Warehouse();
            modelMapper.map(warehouseDTO, warehouse);
            warehouse.setIdWarehouse( id.generateNextID());
            if(!isExitWarehouseByName(warehouseDTO.getName()))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Warehouse name already exists", "")
                );

            Warehouse savedWarehouse = warehouseRepository.save(warehouse);
            logger.info("Warehouse saved successfully with ID: " + savedWarehouse.getIdWarehouse());

            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "Warehouse created successfully", modelMapper.map(savedWarehouse, WarehouseDTOResponse.class))
            );
        } catch (Exception e) {
            logger.error("Error creating warehouse: ", e);

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> createWarehouseByExcel(Sheet sheet) {
        try {
            logger.info("===========================Attempting to create warehouses by Excel");
            List<Warehouse> warehouseList = new ArrayList<>();
            for (Row row : sheet) {
                if (row.getRowNum() == 0) {
                    continue; // Bỏ qua hàng tiêu đề (nếu có)
                }
                IDGenerator id = new IDGenerator("WH", getID());
                Warehouse warehouse = new Warehouse(id);
                if(!isExitWarehouseByName(row.getCell(0).getStringCellValue()))
                    continue;
                warehouse.setName(row.getCell(0).getStringCellValue());
                warehouse.setAddress(row.getCell(1).getStringCellValue());
                warehouse.setInformation(row.getCell(2).getStringCellValue());
                Warehouse warehouse1 = warehouseRepository.save(warehouse);
                warehouseList.add(warehouse);
                logger.info("Warehouse created successfully" + warehouse1.getIdWarehouse());
            }
            logger.info("Number of warehouses " + warehouseList.size());
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "Insert warehouse successfully", warehouseMapper.toDto(warehouseList))
            );
        } catch (Exception e) {
            logger.error("Error inserting warehouse: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }

    }

    @Override
    public ResponseEntity<?> updateWarehouse(WarehouseDTORequest warehouseDTO) {
        try {
            if(warehouseDTO.getIdWarehouse().isEmpty()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Warehouse ID is not blank", "")
                );
            }
            Optional<Warehouse> warehouseOptional = warehouseRepository.findById(warehouseDTO.getIdWarehouse());
            if(!warehouseOptional.isPresent()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Warehouse with id " + warehouseDTO.getIdWarehouse() + " not found" , "")
                );
            }
            Warehouse warehouse = warehouseOptional.get();
            if(warehouse.getIs_delete()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Warehouse with id " + warehouseDTO.getIdWarehouse() + "has been deleted" , "")
                );
            }
            if(!warehouseDTO.getName().isEmpty()){warehouse.setName(warehouseDTO.getName());}
            if(!warehouseDTO.getAddress().isEmpty()){warehouse.setAddress(warehouseDTO.getAddress());}
            if(!warehouseDTO.getInformation().isEmpty()){warehouse.setInformation(warehouseDTO.getInformation());}
            Warehouse w = warehouseRepository.save(warehouse);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "Warehouse updated successfully", modelMapper.map(w, WarehouseDTOResponse.class))
            );
        }
        catch (Exception e) {
            logger.error("Error updating warehouse: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> deleteWarehouse(String warehouseId) {
        logger.info("===========================Attempting to delete warehouse with id: " +  warehouseId);

        Optional<Warehouse> warehouseOptional = warehouseRepository.findById(warehouseId);
        if(!warehouseOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(false, "Warehouse with id " + warehouseId + " not found" , "")
            );
        }
        Warehouse w = warehouseOptional.get();
        // warehouse has been deleted
        if(w.getIs_delete()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(false, "Warehouse with id " + warehouseId + " has been deleted", "")
            );
        }
        try {
            warehouseRepository.delete(w);
            boolean exists = warehouseRepository.existsById(warehouseId);
            if (!exists) {
                logger.info("Warehouse with id {} successfully deleted", warehouseId);
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ObjectResponse(true, "Warehouse deleted successfully", "")
                );
            }
        }catch (Exception e) {
            // Ensure the warehouse is marked as deleted even if delete operation fails
            w.setIs_delete(Boolean.TRUE);
            warehouseRepository.save(w);

            Optional<Warehouse> warehouseOptional1 = warehouseRepository.findById(warehouseId);
            if (!warehouseOptional1.isPresent() || Boolean.TRUE.equals(warehouseOptional1.get().getIs_delete())) {
                logger.info("Warehouse with id {} successfully deleted", warehouseId);
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ObjectResponse(true, "Warehouse deleted successfully", "")
                );
            }
        }
        logger.error("Failed to delete warehouse with id " +  warehouseId);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                new ObjectResponse(false, "Failed to delete warehouse", "")
        );

    }

    @Override
    public ResponseEntity<?> getAllWarehouses() {
        logger.info("============================Attempting to show all warehouse" );
        try {
            List<Warehouse> warehouses = warehouseRepository.findAll();
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "", warehouseMapper.toDto(warehouses))
            );
        } catch (Exception e) {
            logger.error("Error show all warehouse: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public Page<WarehouseDTOResponse> advanceSearch(SearchWarehouseRequest searchWarehouseRequest, Pageable pageable) throws Exception {
        logger.info("==============================Attempting to search for warehouses by page" );
        if (searchWarehouseRequest != null) {
            List<Specification<Warehouse>> specList = getAdvanceSearchSpecList(searchWarehouseRequest);
            logger.info("specList " + specList );
            if (!specList.isEmpty()) {
                Specification<Warehouse> spec = specList.get(0);
                for (int i = 1; i < specList.size(); i++) {
                    spec = spec.and(specList.get(i));
                }
                Page<Warehouse> page = warehouseRepository.findAll(spec, pageable);
                List<WarehouseDTOResponse> ls = new ArrayList<>();
                for (Warehouse warehouse : page) {
                    WarehouseDTOResponse warehouseDTOResponse = new WarehouseDTOResponse();
                    BeanUtils.copyProperties(warehouse, warehouseDTOResponse);
                    ls.add(warehouseDTOResponse);
                }
                long totalElements = page.getTotalElements();
                return new PageImpl<>(ls, page.getPageable(), totalElements);
            }
        }
        return findAll(pageable);
    }

    @Override
    public Warehouse isExitWarehouse(String idWarehouse) {
        if(warehouseRepository.findById(idWarehouse).isPresent()){
            Optional<Warehouse> warehouse = warehouseRepository.findById(idWarehouse);
            return warehouse.get();
        }
        return null;
    }

    @Override
    public Page<WarehouseDTOResponse> findAll(Pageable pageable) {
        Page<Warehouse> page = warehouseRepository.findAll(pageable);
        List<WarehouseDTOResponse> ls = new ArrayList<>();
        for (Warehouse u : page) {
            WarehouseDTOResponse warehouseDTOResponse = new WarehouseDTOResponse();
            BeanUtils.copyProperties(u, warehouseDTOResponse);
            ls.add(warehouseDTOResponse);
        }
        long totalElements = page.getTotalElements();
        logger.info("totalElements {} and page {}", totalElements, page.getTotalElements());
        return new PageImpl<>(ls, page.getPageable(), totalElements);
    }

    private boolean isExitWarehouseByName(String name) {
        Warehouse warehouse = warehouseRepository.findByName(name);
        return warehouse != null ? false : true;
    }


    private List<Specification<Warehouse>> getAdvanceSearchSpecList(@Valid SearchWarehouseRequest s) {
        List<Specification<Warehouse>> specList = new ArrayList<>();
        // advance
        if (Validator.isHaveDataString(s.getName())) {
            specList.add(SearchUtil.like("name", "%" + s.getName() + "%"));
        }
        if(Validator.isHaveDataString(s.getAddress())){
            specList.add(SearchUtil.like("address","%" + s.getAddress() + "%" ));
        }
        if (Validator.isHaveDataString(s.getInformation())) {
            specList.add(SearchUtil.like("information","%" + s.getInformation() + "%" ));
        }
        return specList;
    }

    public int getID(){
        int numberOfExisting = warehouseRepository.countAllWareHouses();
        if(numberOfExisting == 0){
            numberOfExisting = 1;
        }else {
            Warehouse last = warehouseRepository.findTopByOrderByIdWarehouseDesc();

            String lastProductId = last != null ? last.getIdWarehouse() : "WH0";
            int lastNumber = Integer.parseInt(lastProductId.replace("WH", ""));

            // Tăng số lượng lên 1 để tạo ID mới
            int newNumber = lastNumber + 1;
            numberOfExisting = newNumber;
        }
        return numberOfExisting;
    }
}
