package com.api19_4.api19_4.services.servicePostgres;

import com.api19_4.api19_4.dto.request.CreateSupplierDTORequest;
import com.api19_4.api19_4.dto.request.SupplierDTORequest;
import com.api19_4.api19_4.entity.entityPostgres.Supplier;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface SupplierService {
    ResponseEntity<?> create(CreateSupplierDTORequest supplierDTO);
    ResponseEntity<?> createByExcel(Sheet sheet);
    ResponseEntity<?> getAllSupplier();
    ResponseEntity<?> update(SupplierDTORequest supplierDTO);
    ResponseEntity<?> delete(String idSupplier);
    Supplier isExitSupplier(String idSupplier);

}
