package com.api19_4.api19_4.services.servicePostgres;

import com.api19_4.api19_4.dto.request.ShoeProductDtoRequest;
import com.api19_4.api19_4.dto.request.SearchShoeProductRequest;
import com.api19_4.api19_4.dto.response.ShoeProductDtoResponse;
import com.api19_4.api19_4.entity.entityPostgres.ShoeProduct;

import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;
import java.util.List;

public interface ShoeProductService {
    ShoeProduct isExitShoeById(String idProd);
    void updateColor(String color, String idProduct);
    void updateSize(int size, String idProduct);
    ResponseEntity<?> createShoeProduct(ShoeProductDtoRequest productDtoRequest);
    ResponseEntity<?> createByExcelShoe(Sheet sheet);
    ResponseEntity<?> updateProductShoe(ShoeProductDtoRequest productDtoRequest);
    ResponseEntity<?> deleteProductShoe(String idProd);
    ResponseEntity<?> getAllProductShoe();
    ResponseEntity<?> getAllProductCouponsShoe();
    Page<ShoeProductDtoResponse> findAllShoe(Pageable pageable) throws Exception;
    Page<ShoeProductDtoResponse> advanceSearchShoe(@Valid SearchShoeProductRequest searchProductRequest, Pageable pageable) throws Exception ;

}
