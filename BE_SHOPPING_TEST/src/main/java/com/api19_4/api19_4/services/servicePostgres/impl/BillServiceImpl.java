package com.api19_4.api19_4.services.servicePostgres.impl;

import com.api19_4.api19_4.dto.request.BillDTORequest;
import com.api19_4.api19_4.dto.response.BillDTOResponse;
import com.api19_4.api19_4.entity.entityPostgres.Bill;
import com.api19_4.api19_4.model.BillMapper;
import com.api19_4.api19_4.dto.response.ObjectResponse;
import com.api19_4.api19_4.repositories.repoPostgres.BillRepository;
import com.api19_4.api19_4.services.servicePostgres.BillItemService;
import com.api19_4.api19_4.services.servicePostgres.BillService;
import com.api19_4.api19_4.services.servicePostgres.UserService;
import com.api19_4.api19_4.util.IDGenerator;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class BillServiceImpl implements BillService {
    private Logger logger = LoggerFactory.getLogger(BillServiceImpl.class);
    @Autowired
    private BillRepository billRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private BillMapper billMapper;
    @Autowired
    private BillItemService billItemService;

    @Override
    public ResponseEntity<?> createBill(BillDTORequest billDTORequest) throws Exception {
        IDGenerator id = new IDGenerator("BI", getID());
        Bill bill = modelMapper.map(billDTORequest, Bill.class);
        bill.setIdBill(id.generateNextID());
        if (userService.isExitUser(billDTORequest.getIdUser()) == null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(
                    new ObjectResponse(false, "User with id " + billDTORequest.getIdUser() + " not found", "")
            );
        }
        bill.setUser(userService.isExitUser(billDTORequest.getIdUser()));
        bill.setBillItems(billItemService.createListBillItem(billDTORequest.getBillItemDTORequests(), bill));
        Bill saveBill = billRepository.save(bill);
        logger.info("=================== Save Bill=================");
        logger.info("bill " + modelMapper.map(saveBill, BillDTOResponse.class));
        return ResponseEntity.status(HttpStatus.CREATED).body(
                new ObjectResponse(true, "Batch created successfully.", billMapper.toDto(saveBill))
        );
    }

    @Override
    public ResponseEntity<?> updateBill(BillDTORequest request) {
        Bill bill = billRepository.findById(request.getIdBill())
                .orElseThrow(() -> new RuntimeException("Bill with id " + request.getIdBill() + " not found"));
        if (request.getIdUser() != null) {
            if (userService.isExitUser(request.getIdUser()) == null) {
                return ResponseEntity.status(HttpStatus.CREATED).body(
                        new ObjectResponse(false, "User with id " + request.getIdUser() + " not found", "")
                );
            }
            bill.setUser(userService.isExitUser(request.getIdUser()));
        }
        if (request.getBillItemDTORequests() != null) {
            billItemService.updateListBillItem(request.getBillItemDTORequests());
        }
        if (request.getNote() != null) {
            bill.setNote(request.getNote());
        }
        if (request.getStatus() != null) {
            bill.setStatus(request.getStatus());
        }
        if (request.getAddressCustomer() != null) {
            bill.setAddressCustomer(request.getAddressCustomer());
        }
        if (request.getNumberPhoneCustomer() != null) {
            bill.setNumberPhoneCustomer(request.getNumberPhoneCustomer());
        }
        if (request.getShippingFee() != 0) {
            bill.setShippingFee(request.getShippingFee());
        }
        if (request.getPayableAmount() != 0) {
            bill.setPayableAmount(request.getPayableAmount());
        }
        if (request.getTotalPayment() != 0) {
            bill.setTotalPayment(request.getTotalPayment());
        }
        if (request.getDateTimeOrder() != null) {
            bill.setDateTimeOrder(request.getDateTimeOrder());
        }
        Bill b = billRepository.save(bill);
        return ResponseEntity.status(HttpStatus.OK).body(
                new ObjectResponse(true, "Bill updated successfully.", billMapper.toDto(b))
        );

    }

//    @Override
//    public ResponseEntity<?> getBillByIdUser(String idUser) {
//        if(userService.isExitUser(idUser) == null){
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
//                    new ObjectResponse(false, "User with id " + idUser + " not found","")
//            );
//        }
//        List<Bill> billList = billRepository.findBillsByIdUser(idUser);
//        return ResponseEntity.status(HttpStatus.OK).body(
//                new ObjectResponse(true, "",billMapper.toDto(billList))
//        );
//    }

    @Override
    public ResponseEntity<?> deleteBill(String idBatch) {
        return null;
    }

    @Override
    public ResponseEntity<?> getAllBill() {
        return null;
    }

    public int getID() {
        int numberOfExisting = (int) billRepository.count();
        if (numberOfExisting == 0) {
            numberOfExisting = 1;
        } else {
            Bill last = billRepository.findTopByOrderByIdBillDesc();

            String lastId = last != null ? last.getIdBill() : "BI0";
            int lastNumber = Integer.parseInt(lastId.replace("BI", ""));

            // Tăng số lượng lên 1 để tạo ID mới
            int newNumber = lastNumber + 1;
            numberOfExisting = newNumber;
        }
        return numberOfExisting;
    }

}
