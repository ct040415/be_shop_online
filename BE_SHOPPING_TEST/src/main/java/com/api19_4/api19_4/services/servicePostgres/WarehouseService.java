package com.api19_4.api19_4.services.servicePostgres;

import com.api19_4.api19_4.dto.request.CreateWarehouseDTORequest;
import com.api19_4.api19_4.dto.request.SearchWarehouseRequest;
import com.api19_4.api19_4.dto.request.WarehouseDTORequest;
import com.api19_4.api19_4.dto.response.WarehouseDTOResponse;
import com.api19_4.api19_4.entity.entityPostgres.Warehouse;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;

public interface WarehouseService {
    ResponseEntity<?> createWarehouse(CreateWarehouseDTORequest request);
    ResponseEntity<?> createWarehouseByExcel(Sheet sheet);
    ResponseEntity<?> updateWarehouse(WarehouseDTORequest warehouseDTO_request);
    ResponseEntity<?> deleteWarehouse(String warehouseId);
    ResponseEntity<?> getAllWarehouses();
    Page<WarehouseDTOResponse> findAll(Pageable pageable) throws Exception;
    Page<WarehouseDTOResponse> advanceSearch(@Valid SearchWarehouseRequest searchWarehouseRequest, Pageable pageable) throws Exception ;

    Warehouse isExitWarehouse(String idWarehouse);
}
