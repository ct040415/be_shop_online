package com.api19_4.api19_4.services.servicePostgres;

import com.api19_4.api19_4.dto.request.UserDTOChangePassRequest;
import com.api19_4.api19_4.dto.request.UserDTOInfoRequest;
import com.api19_4.api19_4.dto.request.UserDtoLoginRequest;
import com.api19_4.api19_4.dto.request.UserDtoSignUpRequest;
import com.api19_4.api19_4.entity.entityPostgres.UserInfo;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;


public interface UserService {

    UserInfo isExitUser(String id);
    ResponseEntity<?> signUpUser(@Valid UserDtoSignUpRequest userDto);
    ResponseEntity<?> signupByAdmin(@Valid UserDtoSignUpRequest userDtoSignInRequest);
    ResponseEntity<?> loginUser(@Valid UserDtoLoginRequest loginDto);
    ResponseEntity<?> updateUser(@Valid UserDTOInfoRequest userDTOInfoRequest) ;
    ResponseEntity<?> changePass(@Valid UserDTOChangePassRequest userDTOChangePassRequest);
    ResponseEntity<?> getAllUser();
    ResponseEntity<?> getUserById(String idUser);
    ResponseEntity<?> getUserByEMail(@Valid String email);
    ResponseEntity<?>deleteUser(String idUser);
}
