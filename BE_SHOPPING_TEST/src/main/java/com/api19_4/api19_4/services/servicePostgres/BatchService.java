package com.api19_4.api19_4.services.servicePostgres;

import com.api19_4.api19_4.dto.request.BatchDTORequest;
import com.api19_4.api19_4.dto.request.CreateBatchDTORequest;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface BatchService {
    ResponseEntity<?> createBatch(CreateBatchDTORequest batchDTORequest);
    ResponseEntity<?> updateBatch(BatchDTORequest batchDTORequest);
    ResponseEntity<?> deleteBatch(String idBatch);
    void deleteBatchByIdWarehouse(String idWarehouse);
    ResponseEntity<?> getAllBatch();
}
