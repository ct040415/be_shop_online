package com.api19_4.api19_4.services.servicePostgres.impl;

import com.api19_4.api19_4.dto.response.ObjectResponse;
import com.api19_4.api19_4.entity.entityPostgres.Product;
import com.api19_4.api19_4.model.ProductMapper;
import com.api19_4.api19_4.repositories.repoPostgres.ProductRepository;
import com.api19_4.api19_4.services.servicePostgres.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductMapper productMapper;

    @Override
    public String isTypeProduct(String idProd) {
        Optional<Product> product = productRepository.findByIdProd(idProd);
        if(product.isPresent()){
            if(product.get().getProductType().equals("SHOE")){
                return "SHOE";
            }
        }
        return null;
    }

    @Override
    public boolean isIdProduct(String idProd) {
        Optional<Product> productOptional = productRepository.findByIdProd(idProd);
        return productOptional.isPresent();
    }

    @Override
    public ResponseEntity<?> getAllProduct() {
        List<Product> list = productRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(
                new ObjectResponse(true, "", productMapper.toDto(list))
        );
    }


}
