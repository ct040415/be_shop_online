package com.api19_4.api19_4.services.servicePostgres.impl;


import com.api19_4.api19_4.entity.entityPostgres.UserInfo;
import com.api19_4.api19_4.model.UserInfoUserDetails;
import com.api19_4.api19_4.repositories.repoPostgres.UserRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserInfoUserDetailsService implements UserDetailsService {

    @Autowired

    private UserRepositories repositories;

    @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfo user = repositories.findByUserName(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found: " + username);
        }

        return new UserInfoUserDetails(user);
    }

}