package com.api19_4.api19_4.services.servicePostgres;

import com.api19_4.api19_4.dto.request.BillDTORequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface BillService {
    ResponseEntity<?> createBill(BillDTORequest billDTORequest) throws Exception;
    ResponseEntity<?> updateBill(BillDTORequest billDTORequest);
//    ResponseEntity<?> getBillByIdUser(String idUser);
    ResponseEntity<?> deleteBill(String idBatch);
    ResponseEntity<?> getAllBill();
}
