package com.api19_4.api19_4.services.servicePostgres;


import com.api19_4.api19_4.entity.entityPostgres.Cart;

import java.util.List;
import java.util.Optional;

public interface ShoppingCartServices {
    List<Cart> getAllShoppingCarts();

    Optional<Cart> findById(String idUser);

    Cart createShoppingCart(Cart shoppingCart);

    Optional<Cart> getShoppingCartById(String id);
}
