//package com.api19_4.api19_4.services.serviceSqlserver;
//
//import com.api19_4.api19_4.dto.request.ProductLikeDTORequest;
//import com.api19_4.api19_4.dto.request.SearchProductLikeRequest;
//import com.api19_4.api19_4.dto.response.ProductLikeDTOResponse;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.stereotype.Service;
//
//import javax.validation.Valid;
//import java.util.List;
//
//
//
//@Service
//public interface ProductLikeService {
//    ProductLikeDTOResponse createProductLike(ProductLikeDTORequest productLikeDTORequest);
//    ProductLikeDTOResponse updateProductLike(ProductLikeDTORequest productLikeDTORequest);
//    void deleteProductLike(String idProd, String idUser);
//    void deleteAllByIdUser(String idUser);
//    boolean checkProductLike(String idProd, String idUser);
//    List<ProductLikeDTOResponse> getAllProductLike();
//    Page<ProductLikeDTOResponse> findAll(Pageable pageable) throws Exception;
//    Page<ProductLikeDTOResponse> advanceSearch(@Valid SearchProductLikeRequest searchProductLikeRequest, Pageable pageable) throws Exception ;
//
//}
