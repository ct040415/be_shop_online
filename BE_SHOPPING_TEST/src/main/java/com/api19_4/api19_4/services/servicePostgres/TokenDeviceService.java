package com.api19_4.api19_4.services.servicePostgres;


import com.api19_4.api19_4.entity.entityPostgres.TokenDevice;

public interface TokenDeviceService {

    void saveTokenDevice(TokenDevice tokenDevice);
    String getTokenByUserId(String idUser);
}
