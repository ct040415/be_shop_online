package com.api19_4.api19_4.services.servicePostgres.impl;

import com.api19_4.api19_4.dto.request.BatchDTORequest;
import com.api19_4.api19_4.dto.request.CreateBatchDTORequest;
import com.api19_4.api19_4.dto.request.CreateBatchItemDTORequest;
import com.api19_4.api19_4.dto.response.BatchDTOResponse;
import com.api19_4.api19_4.dto.response.ObjectResponse;
import com.api19_4.api19_4.entity.entityPostgres.Batch;
import com.api19_4.api19_4.entity.entityPostgres.Product;
import com.api19_4.api19_4.entity.entityPostgres.Supplier;
import com.api19_4.api19_4.entity.entityPostgres.Warehouse;
import com.api19_4.api19_4.repositories.repoPostgres.BatchRepository;
import com.api19_4.api19_4.services.servicePostgres.*;
import com.api19_4.api19_4.util.IDGenerator;
import com.api19_4.api19_4.model.BatchMapper;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class BatchServiceImpl implements BatchService {
    private Logger logger = LoggerFactory.getLogger(BatchServiceImpl.class);
    @Autowired
    private BatchRepository batchRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private WarehouseService warehouseService;
    @Autowired
    private BatchMapper batchMapper;
    @Autowired
    private BatchItemService batchItemService;
    @Autowired
    private ProductService productService;

    @Override
    public ResponseEntity<?> createBatch(CreateBatchDTORequest batchDTORequest) {
        logger.info("============================= create batch =========================");
        String check = CheckRequest(batchDTORequest);
        if(check != null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(true, check, "")
            );
        }
        IDGenerator id = new IDGenerator("BT", getID());
        Batch batch = new Batch(id);
        batch.setName(batchDTORequest.getName());
        batch.setOrderInitiator(batchDTORequest.getOrderInitiator());
        batch.setDateImport(batchDTORequest.getDateImport());
        batch.setExpirationDate(batchDTORequest.getExpirationDate());
        batch.setManufacturingDate(batchDTORequest.getManufacturingDate());
        batch.setPaidAmount(batchDTORequest.getPaidAmount());
        batch.setPayableAmount(batchDTORequest.getPayableAmount());
        batch.setSupplier(supplierService.isExitSupplier(batchDTORequest.getIdSupplier()));
        batch.setWarehouse(warehouseService.isExitWarehouse(batchDTORequest.getIdWarehouse()));
        logger.info("batch current " + batch.toString());
        batch.setBatchItems(batchItemService.createListBatchItem(batchDTORequest.getBatchItemDTORequests(), batch, batchDTORequest.getIdWarehouse(), batchDTORequest.getIdSupplier()));
        Batch savedBatch = batchRepository.save(batch);
        return ResponseEntity.status(HttpStatus.CREATED).body(
                new ObjectResponse(true, "Batch created successfully.", savedBatch)
        );
    }

    private String CheckRequest(CreateBatchDTORequest batchDTORequest) {
        if(batchDTORequest.getBatchItemDTORequests().isEmpty())
            return "List batchItem cannot be empty";
        if (supplierService.isExitSupplier(batchDTORequest.getIdSupplier()) == null)
            return "Supplier with id " + batchDTORequest.getIdSupplier() + " not found";
        if (warehouseService.isExitWarehouse(batchDTORequest.getIdWarehouse()) == null)
            return "Batch with id " + batchDTORequest.getIdWarehouse() + " not found";
        List<CreateBatchItemDTORequest> list = batchDTORequest.getBatchItemDTORequests();
        if(list.isEmpty())
            return "ListBatchItem cannot be empty";
        for(CreateBatchItemDTORequest bi : list){
            if(!productService.isIdProduct(bi.getIdProduct()))
                return "Product with ID " + bi.getIdProduct() + " not found ";
        }
        return null;
    }


    @Override
    public ResponseEntity<?> updateBatch(BatchDTORequest batchDTORequest) {
        Batch batch = batchRepository.findById(batchDTORequest.getIdWarehouse())
                .orElseThrow(() -> new RuntimeException("Batch with id " + batchDTORequest.getIdBatch() + " not found"));

        if (batchDTORequest.getBatchItemDTORequests() != null) {
            batch.setBatchItems(batchItemService.updateListBatchItem(batchDTORequest.getBatchItemDTORequests()));
        }
        if (batchDTORequest.getName() != null) {
            batch.setName(batchDTORequest.getName());
        }
        if (batchDTORequest.getExpirationDate() != null) {
            batch.setExpirationDate(batchDTORequest.getExpirationDate());
        }
        if (batchDTORequest.getDateImport() != null) {
            batch.setDateImport(batchDTORequest.getDateImport());
        }
        if (batchDTORequest.getManufacturingDate() != null) {
            batch.setManufacturingDate(batchDTORequest.getManufacturingDate());
        }
        if (batchDTORequest.getPaidAmount() != 0) {
            batch.setPaidAmount(batchDTORequest.getPaidAmount());
        }
        if (batchDTORequest.getPayableAmount() != 0) {
            batch.setPayableAmount(batchDTORequest.getPayableAmount());
        }
        if (batchDTORequest.getIdWarehouse() != null) {
            if (warehouseService.isExitWarehouse(batchDTORequest.getIdWarehouse()) == null) {
                return ResponseEntity.status(HttpStatus.CREATED).body(
                        new ObjectResponse(false, "Batch with id " + batchDTORequest.getIdWarehouse() + " not found", "")
                );
            }
            Warehouse warehouse = warehouseService.isExitWarehouse(batchDTORequest.getIdWarehouse());
            batch.setWarehouse(warehouse);
            batch.setWarehouse(warehouse);
        }
        if (batchDTORequest.getIdSupplier() != null) {
            if (supplierService.isExitSupplier(batchDTORequest.getIdSupplier()) == null) {
                return ResponseEntity.status(HttpStatus.CREATED).body(
                        new ObjectResponse(false, "Supplier with id " + batchDTORequest.getIdSupplier() + " not found", "")
                );
            }
            Supplier supplier = supplierService.isExitSupplier(batchDTORequest.getIdSupplier());
            batch.setSupplier(supplier);
        }
        Batch b = batchRepository.save(batch);
        return ResponseEntity.status(HttpStatus.OK).body(
                new ObjectResponse(true, "Batch created successfully.", modelMapper.map(b, BatchDTOResponse.class))
        );
    }

    @Override
    public ResponseEntity<?> deleteBatch(String idBatch) {
        Batch batch = batchRepository.findById(idBatch)
                .orElseThrow(() -> new RuntimeException("Batch with id " + idBatch + " not found"));
        batchRepository.delete(batch);
        return ResponseEntity.status(HttpStatus.OK).body(
                new ObjectResponse(true, "Batch deleted successfully.", "")
        );

    }

    @Override
    public void deleteBatchByIdWarehouse(String idWarehouse) {

    }

    @Override
    public ResponseEntity<?> getAllBatch() {
        List<Batch> batches = batchRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(
                new ObjectResponse(true, "Batch created successfully.", batchMapper.toDto(batches))
        );
    }

    public int getID() {
        int numberOfExisting = batchRepository.countAllBatchs();
        if (numberOfExisting == 0) {
            numberOfExisting = 1;
        } else {
            Batch last = batchRepository.findTopByOrderByIdBatchDesc();

            String lastId = last != null ? last.getIdBatch() : "BT0";
            int lastNumber = Integer.parseInt(lastId.replace("BT", ""));

            // Tăng số lượng lên 1 để tạo ID mới
            int newNumber = lastNumber + 1;
            numberOfExisting = newNumber;
        }
        return numberOfExisting;
    }
}
