package com.api19_4.api19_4.services.servicePostgres.impl;

import com.api19_4.api19_4.dto.request.ShoeProductDtoRequest;
import com.api19_4.api19_4.dto.request.SearchShoeProductRequest;
import com.api19_4.api19_4.dto.response.ShoeProductDtoResponse;

import com.api19_4.api19_4.dto.response.ObjectResponse;
import com.api19_4.api19_4.entity.entityPostgres.Product;
import com.api19_4.api19_4.entity.entityPostgres.ShoeProduct;
import com.api19_4.api19_4.model.ShoeProductMapper;
import com.api19_4.api19_4.repositories.repoPostgres.ShoeProductRepository;
import com.api19_4.api19_4.services.servicePostgres.SupplierService;
import com.api19_4.api19_4.util.IDGenerator;
import com.api19_4.api19_4.services.servicePostgres.ShoeProductService;

import com.api19_4.api19_4.util.SearchUtil;
import com.api19_4.api19_4.util.Validator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ShoeProductServiceImpl implements ShoeProductService {
    private Logger logger = LoggerFactory.getLogger(ShoeProductServiceImpl.class);
    @Autowired
    private ShoeProductRepository shoeProductRepository;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private ShoeProductMapper shoeProductMapper;
    @Autowired
    private ModelMapper modelMapper;


    @Override
    public ShoeProduct isExitShoeById(String idProd) {
        if(shoeProductRepository.findById(idProd).isPresent()){
            return shoeProductRepository.findById(idProd).get();
        }
        return null;
    }

    @Override
    public void updateColor(String color, String idProduct) {
        Optional<ShoeProduct> shoeProductOptional = shoeProductRepository.findById(idProduct);
        if(shoeProductOptional.isPresent()){
            ShoeProduct shoeProduct = shoeProductOptional.get();
            List<String> list = shoeProduct.getColor();
            boolean flag = false;
            for(String c : list){
                if(c.equals(color)){
                    flag = true;
                }
            }
            if(!flag){
                list.add(color);
                shoeProduct.setColor(list);
                shoeProductRepository.save(shoeProduct);
            }
        }
    }

    @Override
    public void updateSize(int size, String idProduct) {
        Optional<ShoeProduct> shoeProductOptional = shoeProductRepository.findById(idProduct);
        if(shoeProductOptional.isPresent()){
            ShoeProduct shoeProduct = shoeProductOptional.get();
            List<Integer> list = shoeProduct.getSize();
            boolean flag = false;
            for(Integer c : list){
                if(c == size){
                    flag = true;
                }
            }
            if(!flag){
                list.add(size);
                shoeProduct.setSize(list);
                shoeProductRepository.save(shoeProduct);
            }
        }
    }

    @Override
    public ResponseEntity<?> createShoeProduct(ShoeProductDtoRequest productDto) {
        IDGenerator id = new IDGenerator("HH", getID());
        if(productDto.getProductType().equals("SHOE")){
            productDto.setIdProd(id.generateNextID());
            ShoeProduct shoeProduct =  modelMapper.map(productDto, ShoeProduct.class);
            if(supplierService.isExitSupplier(productDto.getIdSupplier()) == null){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Supplier with id " + productDto.getIdSupplier() + " not found", "")
                );
            }
            shoeProduct.setSupplier(supplierService.isExitSupplier(productDto.getIdSupplier()));
            ShoeProduct p = shoeProductRepository.save(shoeProduct);
            ShoeProductDtoResponse product= modelMapper.map(p, ShoeProductDtoResponse.class);
            logger.info("Product created successfully " + product.toString());
            return ResponseEntity.status(HttpStatus.CREATED).body(
                    new ObjectResponse(true, "Product created successfully", product)
            );
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ObjectResponse(false, "Failed to insert product.", "")
        );
    }

    @Override
    public ResponseEntity<?> createByExcelShoe(Sheet sheet) {
        List<ShoeProduct> products = new ArrayList<>();
        logger.info("==============Insert Product By Excel============");
        if(checkExcel(sheet) != null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(false, checkExcel(sheet), "")
            );
        }
        int i = 0;
        for (Row row : sheet) {
            i ++;
            if (row.getRowNum() == 0) {
                continue; // Bỏ qua hàng tiêu đề (nếu có)
            }
            IDGenerator id = new IDGenerator("HH", getID());
            ShoeProduct shoeProduct = new ShoeProduct();
            shoeProduct.setIdProd(id.generateNextID());
            shoeProduct.setProductType("SHOE");
            shoeProduct.setBrand(getStringCellValue(row, 0));
            shoeProduct.setProductName(getStringCellValue(row, 1));
            shoeProduct.setRetailPrice((float) getNumericCellValue(row, 2));
            shoeProduct.setCoupons((int) getNumericCellValue(row, 4));
            String idSupplier = getStringCellValue(row, 5);
            logger.info("Id supplier " + idSupplier);
            if(getStringCellValue(row, 5) != null)
                shoeProduct.setSupplier(supplierService.isExitSupplier(getStringCellValue(row, 5)));
            shoeProduct.setSupplier(supplierService.isExitSupplier(idSupplier));
            shoeProduct.setImages(getDetailImages(row, 6, 8));
            String[] colorStrings = getStringCellValue(row, 9).split(",");
            List<String> colors = Arrays.stream(colorStrings)
                    .map(String::trim)
                    .collect(Collectors.toList());
            shoeProduct.setColor(colors);
            ShoeProduct product = shoeProductRepository.save(shoeProduct);
            logger.info("Product " + i + " "  + modelMapper.map(product, ShoeProductDtoResponse.class).toString());
            products.add(product);
        }
        List<ShoeProductDtoResponse> l = shoeProductMapper.toDto(products);
        if(l.isEmpty()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(false, "Failed to insert product.", "")
            );
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(
                new ObjectResponse(true, "Product created successfully", l)
        );
    }

    private String checkExcel(Sheet sheet) {
        // Bắt đầu từ dòng thứ hai (giả sử dòng đầu tiên là header)
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                continue; // Bỏ qua nếu dòng trống
            }

            Cell productNameCell = row.getCell(1); // Cột productName
            Cell retailPriceCell = row.getCell(2); // Cột retailPrice
            Cell idSupplierCell = row.getCell(5);  // Cột idSupplier
            Cell productTypeCell = row.getCell(10); // Cột productType
            Cell couponsCell = row.getCell(4);     // Cột coupons

            if (isCellEmpty(productNameCell)) {
                return "ProductName cannot be empty at row " + (i + 1);
            }else {
                // kiểm tra sản phẩm trùng lặp tên
                String productNameStr = productNameCell.getStringCellValue();
                ShoeProduct shoeProduct = shoeProductRepository.findByProductName(productNameStr);
                if(shoeProduct != null && shoeProduct.getProductName().equals(productNameStr))
                    return "Product name already exists at row " + (i + 1);
            }
            // Kiểm tra nếu các ô bắt buộc bị bỏ trống
            if (isCellEmpty(productTypeCell)) {
                return "ProductType cannot be empty at row " + (i + 1);
            }
            if (isCellEmpty(retailPriceCell)) {
                return "RetailPrice cannot be empty at row " + (i + 1);
            }

            // Kiểm tra nếu retailPrice và coupons có giá trị kiểu chuỗi
            if (retailPriceCell.getCellType() == CellType.STRING) {
                return "RetailPrice must be a number at row " + (i + 1);
            }
            if (couponsCell != null && couponsCell.getCellType() == CellType.STRING) {
                return "Coupons must be a number at row " + (i + 1);
            }

            // kiểm tra nhà cung cấp có tồn tại không
            if(!isCellEmpty(idSupplierCell)) {
                logger.info("Id supplier " + idSupplierCell);
                if (supplierService.isExitSupplier(idSupplierCell.getStringCellValue()) == null) {
                    return "Supplier with id " + idSupplierCell + " not found";
                }
            }
        }
        return null;
    }

    private boolean isCellEmpty(Cell cell) {
        return (cell == null || cell.getCellType() == CellType.BLANK);

    }


    @Override
    public ResponseEntity<?> updateProductShoe(ShoeProductDtoRequest productDto) {
        ShoeProduct shoeProduct = shoeProductRepository.findById(productDto.getIdProd())
                .orElseThrow(() -> new RuntimeException("Product with id " + productDto.getIdProd() + " not found"));

        if(productDto.getProductType() != null){shoeProduct.setProductType(productDto.getProductType());}
        ShoeProduct product1 = shoeProductRepository.save(shoeProduct);
        ShoeProductDtoResponse s = modelMapper.map(product1, ShoeProductDtoResponse.class);
        return ResponseEntity.status(HttpStatus.OK).body(
                new ObjectResponse(true, "Product updated successfully", s)
        );
    }

    @Override
    public ResponseEntity<?> deleteProductShoe(String idProd) {
        ShoeProduct shoeProduct = shoeProductRepository.findById(idProd)
                .orElseThrow(() -> new RuntimeException("Product with id " + idProd + " not found"));
        shoeProductRepository.delete(shoeProduct);
        return ResponseEntity.status(HttpStatus.OK).body(
                new ObjectResponse(true, "Product deleted successfully", "")
        );
    }

//    @Override
//    @Transactional
//    public ResponseEntity<?> getAllProductShoe() {
//        List<ShoeProduct> products = shoeProductRepository.findAll();
//        return ResponseEntity.status(HttpStatus.OK).body(
//                new ObjectResponse(true, "", products)
//        );
//    }

    @Override
    public ResponseEntity<?> getAllProductShoe() {
        List<ShoeProduct> products = shoeProductRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(
                new ObjectResponse(true, "", shoeProductMapper.toDto(products))
        );
    }


    @Override
    public ResponseEntity<?> getAllProductCouponsShoe() {
        List<ShoeProduct> products = shoeProductRepository.findByCouponsNot(0);
        return ResponseEntity.status(HttpStatus.OK).body(
                new ObjectResponse(true, "", shoeProductMapper.toDto(products))
        );
    }

    @Override
    @Transactional
    public Page<ShoeProductDtoResponse> advanceSearchShoe(@Valid SearchShoeProductRequest searchProductRequest, Pageable pageable) throws Exception {
        if (searchProductRequest != null) {
            List<Specification<ShoeProduct>> specList = getAdvanceSearchSpecList(searchProductRequest);
            if (!specList.isEmpty()) {
                Specification<ShoeProduct> spec = specList.get(0);
                for (int i = 1; i < specList.size(); i++) {
                    spec = spec.and(specList.get(i));
                }
                Page<ShoeProduct> page = shoeProductRepository.findAll(spec, pageable);
                List<ShoeProductDtoResponse> ls = new ArrayList<>();
                for (ShoeProduct shoeProduct : page) {
                    ShoeProductDtoResponse productDtoResponse = new ShoeProductDtoResponse();
                    BeanUtils.copyProperties(shoeProduct, productDtoResponse);
                    ls.add(productDtoResponse);
                }
                long totalElements = page.getTotalElements();
                return new PageImpl<>(ls, page.getPageable(), totalElements);
            }
        }
        return findAllShoe(pageable);
    }

    @Override
    public Page<ShoeProductDtoResponse> findAllShoe(Pageable pageable) {
        Page<ShoeProduct> page = shoeProductRepository.findAll(pageable);
        List<ShoeProductDtoResponse> ls = new ArrayList<>();
        for (Product u : page) {
            ShoeProductDtoResponse productDtoResponse = new ShoeProductDtoResponse();
            BeanUtils.copyProperties(u, productDtoResponse);
            ls.add(productDtoResponse);
        }
        long totalElements = page.getTotalElements();
        return new PageImpl<>(ls, page.getPageable(), totalElements);
    }

    private List<Specification<ShoeProduct>> getAdvanceSearchSpecList(@Valid SearchShoeProductRequest s) {
        List<Specification<ShoeProduct>> specList = new ArrayList<>();
        // advance
        if (Validator.isHaveDataString(s.getProductName())) {
            specList.add(SearchUtil.like("productName", "%" + s.getProductName() + "%"));
        }
        if(Validator.isHaveDataString(s.getProductType())){
            specList.add(SearchUtil.like("productType","%" + s.getProductType() + "%" ));
        }
        if (Validator.isHaveDataString(s.getBrand())) {
            specList.add(SearchUtil.like("brand","%" + s.getBrand() + "%" ));
        }
        return specList;
    }

    public int getID(){
        int numberOfExisting = shoeProductRepository.countAllShoeProducts();
        if(numberOfExisting == 0){
            numberOfExisting = 1;
        }else {
            ShoeProduct last = shoeProductRepository.findTopByOrderByIdProdDesc();

            String lastProductId = last != null ? last.getIdProd() : "HH0";
            int lastNumber = Integer.parseInt(lastProductId.replace("HH", ""));

            // Tăng số lượng lên 1 để tạo ID mới
            int newNumber = lastNumber + 1;
            numberOfExisting = newNumber;
        }
        return numberOfExisting;
    }



    private String getStringCellValue(Row row, int cellIndex) {
        return row.getCell(cellIndex) != null ? row.getCell(cellIndex).getStringCellValue() : "";
    }

    private double getNumericCellValue(Row row, int cellIndex) {
        return row.getCell(cellIndex) != null ? row.getCell(cellIndex).getNumericCellValue() : 0;
    }

    private List<String> getDetailImages(Row row, int startCellIndex, int endCellIndex) {
        List<String> images = new ArrayList<>();
        for (int i = startCellIndex; i <= endCellIndex; i++) {
            String imageUrl = getStringCellValue(row, i);
           images.add(imageUrl);
        }
        return images;
    }
}
