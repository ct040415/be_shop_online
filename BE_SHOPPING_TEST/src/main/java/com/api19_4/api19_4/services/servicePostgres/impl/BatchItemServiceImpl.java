package com.api19_4.api19_4.services.servicePostgres.impl;

import com.api19_4.api19_4.dto.request.BatchItemDTORequest;
import com.api19_4.api19_4.dto.request.CreateBatchItemDTORequest;
import com.api19_4.api19_4.entity.entityPostgres.*;
import com.api19_4.api19_4.repositories.repoPostgres.BatchItemRepository;
import com.api19_4.api19_4.services.servicePostgres.*;
import com.api19_4.api19_4.util.IDGenerator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BatchItemServiceImpl implements BatchItemService {
    @Autowired
    private BatchItemRepository batchItemRepository;
    @Autowired
    private ProductService productService;
    @Autowired
    private ShoeProductService shoeProductService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private WarehouseService warehouseService;
    @Override
    public List<BatchItem> createListBatchItem(List<CreateBatchItemDTORequest> listRequest, Batch batch, String idWH, String idSP) {
        List<BatchItem> batchItems = new ArrayList<>();
        for (CreateBatchItemDTORequest b : listRequest) {
            if (b != null) {
                // Generate BatchItem ID
                IDGenerator id = new IDGenerator("IB", getID());
                BatchItem batchItem = new BatchItem(id);

                // Set BatchItem details
                batchItem.setPurchaseUnitPrice(b.getPurchaseUnitPrice());
                batchItem.setRetailUnitPrice(b.getRetailUnitPrice());
                batchItem.setDiscount(b.getDiscount());
                batchItem.setStatus(b.getStatus());
                batchItem.setNote(b.getNote());
                batchItem.setTotalQuantity(b.getTotalQuantity());
                batchItem.setTotalPurchasePrice(b.getTotalPurchasePrice());

                // Handle SHOE product type
                if ("SHOE".equals(productService.isTypeProduct(b.getIdProduct()))) {
                    String color = b.getColor();
                    int size = b.getSize();
                    // Update ShoeProduct details
                    shoeProductService.updateColor(color, b.getIdProduct());
                    shoeProductService.updateSize(size, b.getIdProduct());
                    // Set BatchItem color, size, and ShoeProduct
                    batchItem.setColor(color);
                    batchItem.setSize(size);
                    ShoeProduct shoeProduct = shoeProductService.isExitShoeById(b.getIdProduct());
                    batchItem.setProduct(shoeProduct);
                }

                // Set Batch reference and save BatchItem
                batchItem.setBatch(batch);
                BatchItem savedBatchItem = batchItemRepository.save(batchItem);
                batchItems.add(savedBatchItem);
            }
        }
        return batchItems;
    }

    @Override
    public List<BatchItem> updateListBatchItem(List<BatchItemDTORequest> listRequest) {
        List<BatchItem> l = new ArrayList<>();
        for (BatchItemDTORequest b : listRequest) {
            BatchItem batchItem = batchItemRepository.findById(b.getIdBatchItem()).orElseThrow(() -> new RuntimeException("BatchItem not found"));
            if (b.getIdProduct() != null) {
                if (productService.isTypeProduct(b.getIdProduct()) != null) {
                    if (productService.isTypeProduct(b.getIdProduct()).equals("SHOE")) {
                        ShoeProduct shoeProduct = shoeProductService.isExitShoeById(b.getIdProduct());
                        batchItem.setProduct(shoeProduct);
                    }
                }
            }
            if (b.getNote() != null) {
                batchItem.setNote(b.getNote());
            }
            if (b.getSize() != 0) {
                batchItem.setSize(b.getSize());
            }
            if (b.getDiscount() != 0) {
                batchItem.setDiscount(b.getDiscount());
            }
            if (b.getStatus() != null) {
                batchItem.setStatus(b.getStatus());
            }
            if (b.getTotalQuantity() != 0) {
                batchItem.setTotalQuantity(b.getTotalQuantity());
            }
            BatchItem bi = batchItemRepository.save(batchItem);
            l.add(bi);
        }
        return l;
    }

    public int getID() {
        int numberOfExisting = batchItemRepository.countAllBatchItems();
        if (numberOfExisting == 0) {
            numberOfExisting = 1;
        } else {
            BatchItem last = batchItemRepository.findTopByOrderByIdBatchItemDesc();

            String lastId = last != null ? last.getIdBatchItem() : "IB0";
            int lastNumber = Integer.parseInt(lastId.replace("IB", ""));

            // Tăng số lượng lên 1 để tạo ID mới
            int newNumber = lastNumber + 1;
            numberOfExisting = newNumber;
        }
        return numberOfExisting;
    }
}
