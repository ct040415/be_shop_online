package com.api19_4.api19_4.services.servicePostgres;

import com.api19_4.api19_4.entity.entityPostgres.Product;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public interface ProductService {
    String isTypeProduct(String idProd);
    boolean isIdProduct(String idProd);
    ResponseEntity<?> getAllProduct();

}
