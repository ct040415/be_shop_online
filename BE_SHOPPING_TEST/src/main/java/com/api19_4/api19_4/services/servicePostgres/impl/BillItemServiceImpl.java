package com.api19_4.api19_4.services.servicePostgres.impl;

import com.api19_4.api19_4.dto.request.BillItemDTORequest;
import com.api19_4.api19_4.entity.entityPostgres.Bill;
import com.api19_4.api19_4.entity.entityPostgres.BillItem;
import com.api19_4.api19_4.entity.entityPostgres.ShoeProduct;
import com.api19_4.api19_4.repositories.repoPostgres.BillItemRepository;
import com.api19_4.api19_4.services.servicePostgres.BillItemService;
import com.api19_4.api19_4.services.servicePostgres.ProductService;
import com.api19_4.api19_4.services.servicePostgres.ShoeProductService;
import com.api19_4.api19_4.util.IDGenerator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BillItemServiceImpl implements BillItemService {
    @Autowired
    private BillItemRepository billItemRepository;
    @Autowired
    private ProductService productService;
    @Autowired
    private ShoeProductService shoeProductService;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<BillItem> createListBillItem(List<BillItemDTORequest> listRequest, Bill bill) throws Exception {
        List<BillItem> l = new ArrayList<>();
        for (BillItemDTORequest b : listRequest) {
            if (b != null) {
                IDGenerator id = new IDGenerator("BII", getID());
                BillItem billItem = modelMapper.map(b, BillItem.class);
                billItem.setIdBillItem(id.generateNextID());
                if (productService.isTypeProduct(b.getIdProd()).equals("SHOE")) {
                    ShoeProduct shoeProduct = shoeProductService.isExitShoeById(b.getIdProd());
                    if (shoeProduct == null) {
                        throw new Exception("ShoeProduct with id " + b.getIdProd() + " not found");
                    }
                    billItem.setProduct(shoeProduct);
                }
                billItem.setBill(bill);
                BillItem bi = billItemRepository.save(billItem);
                l.add(bi);
            }
        }
        return l;
    }

    @Override
    public List<BillItem> updateListBillItem(List<BillItemDTORequest> listRequest) {
        List<BillItem> l = new ArrayList<>();
        for (BillItemDTORequest b : listRequest) {
            BillItem bi = billItemRepository.findById(b.getIdBillItem()).orElseThrow(() -> new RuntimeException("BillItem not found"));
            if (b.getSize() != 0) {
                bi.setSize(b.getSize());
            }
            if (b.getQuantity() != 0) {
                bi.setQuantity(b.getQuantity());
            }
            if (b.getTotalPriceProd() != 0) {
                bi.setTotalPriceProd(b.getTotalPriceProd());
            }
            BillItem billItem = billItemRepository.save(bi);
            l.add(billItem);
        }
        return l;
    }

    public int getID() {
        int numberOfExisting = (int) billItemRepository.count();
        if (numberOfExisting == 0) {
            numberOfExisting = 1;
        } else {
            BillItem last = billItemRepository.findTopByOrderByIdBillItemDesc();

            String lastId = last != null ? last.getIdBillItem() : "BII0";
            int lastNumber = Integer.parseInt(lastId.replace("BII", ""));

            // Tăng số lượng lên 1 để tạo ID mới
            int newNumber = lastNumber + 1;
            numberOfExisting = newNumber;
        }
        return numberOfExisting;
    }
}
