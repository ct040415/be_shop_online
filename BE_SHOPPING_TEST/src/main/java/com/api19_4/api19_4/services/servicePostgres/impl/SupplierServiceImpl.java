package com.api19_4.api19_4.services.servicePostgres.impl;

import com.api19_4.api19_4.dto.request.CreateSupplierDTORequest;
import com.api19_4.api19_4.dto.request.SupplierDTORequest;
import com.api19_4.api19_4.dto.response.SupplierDTOResponse;
import com.api19_4.api19_4.dto.response.ObjectResponse;
import com.api19_4.api19_4.entity.entityPostgres.Supplier;
import com.api19_4.api19_4.entity.entityPostgres.Warehouse;
import com.api19_4.api19_4.model.SupplierMapper;
import com.api19_4.api19_4.repositories.repoPostgres.SupplierRepository;
import com.api19_4.api19_4.util.IDGenerator;
import com.api19_4.api19_4.services.servicePostgres.SupplierService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SupplierServiceImpl implements SupplierService {
    private Logger logger = LoggerFactory.getLogger(SupplierServiceImpl.class);
    @Autowired
    private SupplierRepository supplierRepository;
    @Autowired
    private SupplierMapper supplierMapper;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ResponseEntity<?> create(CreateSupplierDTORequest supplierDTO) {
        try {
            logger.info("Attempting to create supplier");
            IDGenerator id = new IDGenerator("SP", getID());
            Supplier supplier = new Supplier();
            modelMapper.map(supplierDTO, supplier);
            supplier.setIdSupplier( id.generateNextID());
            if(validateSupplier(supplierDTO) != null){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, validateSupplier(supplierDTO),"")
                );
            }
            Supplier save = supplierRepository.save(supplier);
            logger.info("Supplier saved successfully with ID: " + save.getIdSupplier());

            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "Supplier created successfully", modelMapper.map(save, SupplierDTOResponse.class))
            );
        } catch (Exception e) {
            logger.error("Error creating suppler: ", e);

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    private String validateSupplier(CreateSupplierDTORequest supplierDTO) {
        if(supplierDTO.getName().isEmpty() || supplierDTO.getInformation().isEmpty() || supplierDTO.getAddress().isEmpty()){
            return "The supplier's name, phone number or address must not be blank.";
        }
        Supplier supplierByName = supplierRepository.findByNameSupplier(supplierDTO.getName());
        if(supplierByName != null ){
            return "Supplier name already exists.";
        }
        return null;
    }

    @Override
    public ResponseEntity<?> createByExcel(Sheet sheet) {
        try {
            logger.info("Attempting to create supplier by Excel");
            List<Supplier> supplierList = new ArrayList<>();
            for (Row row : sheet) {
                if (row.getRowNum() == 0) {
                    continue;
                }
                IDGenerator id = new IDGenerator("SP", getID());
                Supplier supplier = new Supplier(id);
                Supplier s = supplierRepository.findByNameSupplier(row.getCell(0).getStringCellValue());
                if(s != null){
                    continue;
                }
                supplier.setName(row.getCell(0).getStringCellValue());
                supplier.setInformation(row.getCell(1).getStringCellValue());
                supplier.setAddress(row.getCell(2).getStringCellValue());
                Supplier save = supplierRepository.save(supplier);
                supplierList.add(supplier);
                logger.info("Supplier created successfully" + save.getIdSupplier());
            }
            logger.info("Number of supplier " + supplierList.size());
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "Insert supplier successfully", supplierMapper.toDto(supplierList))
            );
        } catch (Exception e) {
            logger.error("Error inserting supplier: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> getAllSupplier() {
        logger.info("============================Attempting to show all supplier" );
        try {
            List<Supplier> supplierList = supplierRepository.findAll();
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "", supplierMapper.toDto(supplierList))
            );
        } catch (Exception e) {
            logger.error("Error show all supplier: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> update(SupplierDTORequest supplierDTO) {
        try {
            if(supplierDTO.getIdSupplier().isEmpty()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Supplier ID is not blank", "")
                );
            }
            Optional<Supplier> supplierOptional = supplierRepository.findById(supplierDTO.getIdSupplier());
            if(!supplierOptional.isPresent()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ObjectResponse(false, "Supplier with id " + supplierDTO.getIdSupplier() + " not found", "")
                );
            }
            Supplier supplier = supplierOptional.get();
            if(!supplierDTO.getName().isEmpty()){supplier.setName(supplierDTO.getName());}
            if(!supplierDTO.getAddress().isEmpty()){supplier.setAddress(supplierDTO.getAddress());}
            if(!supplierDTO.getInformation().isEmpty()){supplier.setInformation(supplierDTO.getInformation());}
            Supplier s = supplierRepository.save(supplier);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "Supplier updated successfully", modelMapper.map(s, SupplierDTOResponse.class))
            );
        }
        catch (Exception e) {
            logger.error("Error updating supplier: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Internal Server Error", e.getMessage())
            );
        }
    }

    @Override
    public ResponseEntity<?> delete(String idSupplier) {
        logger.info("Attempting to delete supplier with id: " +  idSupplier);

        Optional<Supplier> supplierOptional = supplierRepository.findById(idSupplier);
        if(!supplierOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ObjectResponse(false, "Supplier with id " +idSupplier + " not found" , "")
            );
        }
        supplierRepository.delete(supplierOptional.get());

        boolean exists = supplierRepository.existsById(idSupplier);
        if (!exists) {
            logger.info("Supplier with id {} successfully deleted", idSupplier);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ObjectResponse(true, "Supplier deleted successfully", "")
            );
        } else {
            logger.error("Failed to delete supplier with id " +  idSupplier);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ObjectResponse(false, "Failed to delete supplier", "")
            );
        }
    }

    @Override
    public Supplier isExitSupplier(String idSupplier) {
        if(supplierRepository.findById(idSupplier).isPresent()){
            Optional<Supplier> supplierOptional = supplierRepository.findById(idSupplier);
            return supplierOptional.get();
        }
        return null;
    }

    public int getID(){
        int numberOfExisting = supplierRepository.countAllSuppliers();
        if(numberOfExisting == 0){
            numberOfExisting = 1;
        }else {
            Supplier last = supplierRepository.findTopByOrderByIdSupplierDesc();

            String lastProductId = last != null ? last.getIdSupplier() : "SP0";
            int lastNumber = Integer.parseInt(lastProductId.replace("SP", ""));

            // Tăng số lượng lên 1 để tạo ID mới
            int newNumber = lastNumber + 1;
            numberOfExisting = newNumber;
        }
        return numberOfExisting;
    }
}
