package com.api19_4.api19_4.services.servicePostgres;

import com.api19_4.api19_4.dto.request.BillItemDTORequest;
import com.api19_4.api19_4.entity.entityPostgres.Bill;
import com.api19_4.api19_4.entity.entityPostgres.BillItem;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BillItemService{
    List<BillItem> createListBillItem (List<BillItemDTORequest> billItemDTORequest, Bill bill) throws Exception;
    List<BillItem> updateListBillItem(List<BillItemDTORequest> billItemDTORequests);
}
