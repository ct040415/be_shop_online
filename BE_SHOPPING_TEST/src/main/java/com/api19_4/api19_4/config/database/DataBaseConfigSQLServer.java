//package com.api19_4.api19_4.config.database;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.core.env.Environment;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.JpaVendorAdapter;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.sql.DataSource;
//import java.util.HashMap;
//
//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(
//        entityManagerFactoryRef = "sqlServerManagerFactory",
//        basePackages = {"com.api19_4.api19_4.repositories.repoSqlserver"},
//        transactionManagerRef = "sqlServerTransactionManager"
//)
//public class DataBaseConfigSQLServer {
//    Logger logger = LoggerFactory.getLogger(DataBaseConfigSQLServer.class);
//    @Autowired
//    Environment env;
//
//    @Primary
//    @Bean(name = "sqlServerDataSource")
//    public DataSource dataSource() {
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setUrl(env.getProperty("spring.datasource.primary.url"));
//        logger.info("URL " + env.getProperty("spring.datasource.primary.url"));
//        dataSource.setUsername(env.getProperty("spring.datasource.primary.username"));
//        dataSource.setPassword(env.getProperty("spring.datasource.primary.password"));
//        dataSource.setDriverClassName(env.getProperty("spring.datasource.primary.driver-class-name"));
//        return dataSource;
//    }
//
//    @Primary
//    @Bean(name = "sqlServerManagerFactory")
//    public LocalContainerEntityManagerFactoryBean entityManager() {
//        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
//        entityManagerFactoryBean.setDataSource(dataSource());
//        JpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
//        entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
//        HashMap<String, Object> properties = new HashMap<>();
//        properties.put("hibernate.hbm2ddl.auto", "update");
//        entityManagerFactoryBean.setJpaPropertyMap(properties);
//        entityManagerFactoryBean.setPackagesToScan("com.api19_4.api19_4.entity.entitySqlserver");
//        return entityManagerFactoryBean;
//    }
//
//    @Primary
//    @Bean("sqlServerTransactionManager")
//    public PlatformTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean entityManagerFactory) {
//        return new JpaTransactionManager(entityManagerFactory.getObject());
//    }
//}